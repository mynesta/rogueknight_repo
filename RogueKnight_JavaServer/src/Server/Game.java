package Server;

/**
 *
 * @author manu and sergio
 */
public class Game {
    
    //variables
    private int gameID;
    private boolean finished;
    private int checkpoint;
    private int goldObtained;
    private int itemsObtained;
    private int finalFloor;
    private int experienceObtained;
    private int enemiesEliminated;
    private int characterPlayableID;
    
    /*
    *constructor class
    */

    /**
     * constructor class
     * @param gameID
     * @param finished
     * @param checkpoint
     * @param goldObtained
     * @param itemsObtained
     * @param finalFloor
     * @param experienceObtained
     * @param enemiesEliminated
     * @param characterPlayableID
     */

    public Game(int gameID,boolean finished,int checkpoint,int goldObtained,
            int itemsObtained,int finalFloor,int experienceObtained,
            int enemiesEliminated,int characterPlayableID){
        this.gameID = gameID;
        this.finished = finished;
        this.checkpoint = checkpoint;
        this.goldObtained = goldObtained;
        this.itemsObtained = itemsObtained;
        this.finalFloor = finalFloor;
        this.experienceObtained = experienceObtained;
        this.enemiesEliminated = enemiesEliminated;
        this.characterPlayableID = characterPlayableID;
    }

    /**
     * get game id
     * @return gameID
     */

    public int getGameID() {
        return gameID;
    }

    /**
     * get total gold obtained
     * @return goldObtained
     */
    public int getGoldObtained() {
        return goldObtained;
    }

    /**
     * get total items obtained
     * @return itemsObtained
     */
    public int getItemsObtained() {
        return itemsObtained;
    }

    /**
     * get final floor
     * @return finalFloor
     */
    public int getFinalFloor() {
        return finalFloor;
    }

    /**
     * get total enemies eliminated
     * @return enemiesEliminated
     */
    public int getEnemiesEliminated() {
        return enemiesEliminated;
    }

    /**
     * get character id
     * @return characterPlayableID
     */
    public int getCharacterPlayableID() {
        return characterPlayableID;
    }

    /**
     * get finished
     * @return finished
     */
    public boolean isFinished() {
        return finished;
    }

    /**
     * get checkpoint
     * @return checkpoint
     */
    public int getCheckpoint() {
        return checkpoint;
    }

    /**
     * get total experience obtained
     * @return experienceObtained
     */
    public int getExperienceObtained() {
        return experienceObtained;
    }
}

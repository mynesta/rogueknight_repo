package Server;

/**
 *
 * @author manu and sergio
 */
public class PlayerInventory {
    //variables
    private int characterPlayableID;
    private int itemID;
    private int quantity;
    

    /**
     * constructor class
     * @param characterPlayableID
     * @param itemID
     * @param quantity
     */

    public PlayerInventory(int characterPlayableID,int itemID,int quantity){
        this.characterPlayableID = characterPlayableID;
        this.itemID = itemID;
        this.quantity = quantity;
    }


    /**
     *getCharacterPlayableID
     * @return characterPlayableID
     */ 

    public int getCharacterPlayableID() {
        return characterPlayableID;
    }

    /**
     * getItemID
     * @return itemID
     */
    public int getItemID() {
        return itemID;
    }
    
    /**
     *getQuantity
     * @return quantity
     */
    public int getQuantity() {
        return quantity;
    }
}

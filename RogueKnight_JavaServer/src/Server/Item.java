package Server;

/**
 *
 * @author manu and sergio
 */
public class Item {
    //variables
    private int itemID;
    private String name;
    private String effect;
    private boolean equip;
    private int buyingCost;
    private int sellingCost;
    

    /**
     * constructor class
     * @param itemID
     * @param name
     * @param effect
     * @param equip
     * @param buyingCost
     * @param sellingCost
     */

    public Item(int itemID,String name,String effect,boolean equip,
            int buyingCost,int sellingCost){
        this.itemID = itemID;
        this.name = name;
        this.effect = effect;
        this.equip = equip;
        this.buyingCost = buyingCost;
        this.sellingCost = sellingCost;
        
    }


    /**
     * getItemID
     * @return itemID
     */

    public int getItemID() {
        return itemID;
    }

    /**
     * getName
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * getEffect
     * @return effect
     */
    public String getEffect() {
        return effect;
    }

    /**
     * isEquip
     * @return equip
     */
    public boolean isEquip() {
        return equip;
    }

    /**
     * getBuyingCost
     * @return buyingCost
     */
    public int getBuyingCost() {
        return buyingCost;
    }

    /**
     *getSellingCost
     * @return sellingCost
     */
    public int getSellingCost() {
        return sellingCost;
    }
    
    
}

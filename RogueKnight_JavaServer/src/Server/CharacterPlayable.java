package Server;

/**
 *
 * @author manu and sergio
 */
public class CharacterPlayable {
    //variables
    private int characterPlayableID;
    private Boolean alive;
    private String nickname;
    private int characterLevel;
    private int hitPoints;
    private int attack;
    private int defense;
    private int inventorySlots;
    private int experience;
    private int needExperience;
    private int gold;
    private String username;
    
    /**
     * constructor class
     * @param characterPlayableID
     * @param alive
     * @param nickname
     * @param characterLevel
     * @param hitPoints
     * @param attack
     * @param defense
     * @param inventorySlots
     * @param experience
     * @param needExperience
     * @param gold
     * @param username
     */

    public CharacterPlayable(int characterPlayableID, Boolean alive, String nickname,
            int characterLevel,int hitPoints,int attack,int defense,int inventorySlots,
            int experience,int needExperience,int gold,String username){
        this.characterPlayableID = characterPlayableID;
        this.alive = alive;
        this.nickname = nickname;
        this.characterLevel = characterLevel;
        this.hitPoints = hitPoints;
        this.attack = attack;
        this.defense = defense;
        this.inventorySlots = inventorySlots;
        this.experience = experience;
        this.needExperience = needExperience;
        this.gold = gold;
        this.username = username;
    }

    /**
     * get character id
     * @return characterPlayableID
     */

    public int getCharacterPlayableID() {
        return characterPlayableID;
    }

    /**
     * get if is alive or not
     * @return alive
     */
    public Boolean isAlive() {
        return alive;
    }

    /**
     * get name
     * @return nickname
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * get level
     * @return characterLevel
     */
    public int getCharacterLevel() {
        return characterLevel;
    }

    /**
     * get life
     * @return hitPoints
     */
    public int getHitPoints() {
        return hitPoints;
    }

    /**
     * get defense
     * @return defense
     */
    public int getDefense() {
        return defense;
    }

    /**
     * get slots inventory
     * @return inventorySlots
     */
    public int getInventorySlots() {
        return inventorySlots;
    }

    /**
     * get experience
     * @return experience
     */
    public int getExperience() {
        return experience;
    }

    /**
     * get gold
     * @return gold
     */
    public int getGold() {
        return gold;
    }

    /**
     * get user name
     * @return username
     */
    public String getUsername() {
        return username;
    }
    
    /**
     * get attack
     * @return attack
     */
    public int getAttack() {
        return attack;
    }
    
    /**
     * get experiento to up leve
     * @return needExperience
     */
    public int getNeedexperience() {
        return needExperience;
    }
}

package Server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author manu and sergio
 */
public class Server {
    //variables
    static int port = 12345;
    static private ArrayList<UserDatabase> users;
    static private ArrayList<CharacterPlayable> charactersPlayables;
    static private ArrayList<Item> items;
    static private ArrayList<PlayerInventory> inventory;
    static private ArrayList<Game> game;
    static DataInputStream dis;
    static DataOutputStream dos;
    static int idGame;
    static int playerID;

    /**
     * contrsuctor class
     * @param args
     */
    public static void main(String[] args) {
        new Server().start();
    }
    //server with threads
    static class ServerThread extends Thread {

        Socket sk;

        ServerThread(Socket sk) {
            this.sk = sk;
        }

        @Override
        public void run() {
            try {
                synchronized ("semaphore") {
                    System.out.println("A client has connected: " + sk.getInetAddress().getHostAddress());
                    //Load communications channels
                    dis = new DataInputStream(sk.getInputStream());
                    dos = new DataOutputStream(sk.getOutputStream());
                    //get user
                    String user = receiveString(dis);
                    String password = receiveString(dis);
                    System.out.println(user+" trying to connect");
                    Boolean userOk = false;
                    //check if the user and password are correct
                    for (int i = 0; i < users.size(); i++) {
                        if (users.get(i).getName().equals(user) && users.get(i).getPassword().equals(password)) {
                            userOk = true;
                            break;
                        }
                    }
                    if (userOk) {
                        //server informs the client that the user is correct
                        System.out.println(user+ " Correct user");
                        sendString("CorrectUser", dos);
                        //client informs the server what to do with database (select,update,insert)
                        String petitionClient = receiveString(dis);
                        System.out.println("Selected action: "+petitionClient);
                        switch (petitionClient) {
                            case "select":
                                //client informs the server what kind of select do you want to make
                                String table = receiveString(dis);
                                System.out.println("table selected: "+table);
                                switch (table) {
                                    case "player":
                                        //reload characters of username
                                        charactersPlayables.clear();
                                        loadInfoDB("SELECT * FROM CHARACTERPLAYABLE WHERE username = ?", receiveString(dis), charactersPlayables, "character");
                                        Boolean alive = false;
                                        for (int i = 0; i < charactersPlayables.size(); i++) {
                                            //searching character alive and send info to the client
                                            if (charactersPlayables.get(i).isAlive()) {
                                                alive = true;
                                                sendString(String.valueOf(alive), dos);
                                                sendString(String.valueOf(charactersPlayables.get(i).getCharacterPlayableID()),dos);
                                                sendString(charactersPlayables.get(i).getNickname(), dos);
                                                sendString(String.valueOf(charactersPlayables.get(i).getCharacterLevel()), dos);
                                                sendString(String.valueOf(charactersPlayables.get(i).getHitPoints()), dos);
                                                sendString(String.valueOf(charactersPlayables.get(i).getAttack()), dos);
                                                sendString(String.valueOf(charactersPlayables.get(i).getDefense()), dos);
                                                sendString(String.valueOf(charactersPlayables.get(i).getInventorySlots()), dos);
                                                sendString(String.valueOf(charactersPlayables.get(i).getExperience()), dos);
                                                sendString(String.valueOf(charactersPlayables.get(i).getNeedexperience()), dos);
                                                sendString(String.valueOf(charactersPlayables.get(i).getGold()), dos);
                                                playerID = charactersPlayables.get(i).getCharacterPlayableID();
                                                System.out.println("ID player selected: "+playerID);
                                                inventory.clear();
                                                //load player inventory of character selected
                                                //send size inventory to the client
                                                loadInfoDB("SELECT * FROM PLAYERINVENTORY WHERE characterPlayableID = ?", String.valueOf(playerID), inventory, "inventory");
                                                sendString(String.valueOf(inventory.size()), dos);
                                                break;
                                            }
                                        }
                                        //If you don't have any characters alive, notify the client
                                        if (!alive) {
                                            sendString(String.valueOf(alive), dos);
                                        }
                                        break;
                                    case "itemInventory":
                                        //reload player inventory from character selected
                                        inventory.clear();
                                        loadInfoDB("SELECT * FROM PLAYERINVENTORY WHERE characterPlayableID = ?", receiveString(dis), inventory, "inventory");
                                        //get iteration from the client (load item)
                                        int itemPosition = Integer.parseInt(receiveString(dis));
                                            for (int k = 0; k < items.size(); k++) {
                                                //send to the client values from items
                                                if (items.get(k).getItemID() == inventory.get(itemPosition).getItemID()) {
                                                    sendString(String.valueOf(inventory.get(itemPosition).getItemID()), dos);
                                                    sendString(items.get(k).getName(), dos);
                                                    sendString(String.valueOf(inventory.get(itemPosition).getQuantity()), dos);
                                                    sendString(String.valueOf(items.get(k).isEquip()), dos);
                                                    break;
                                                }
                                            }
                                        
                                        break;
                                    case "game":
                                        //reload games from user
                                        game.clear();
                                        loadInfoDB("SELECT * FROM GAME WHERE characterPlayableID = ?", String.valueOf(playerID), game, "game");
                                        Boolean finished = true;
                                        for (int i = 0; i < game.size(); i++) {
                                            //send to the client values from game if character is alive (finished)
                                            if (!game.get(i).isFinished()) {
                                                finished = false;
                                                sendString(String.valueOf(game.get(i).getGameID()), dos);
                                                sendString(String.valueOf(game.get(i).getCheckpoint()), dos);
                                                sendString(String.valueOf(game.get(i).getGoldObtained()), dos);
                                                sendString(String.valueOf(game.get(i).getItemsObtained()), dos);
                                                sendString(String.valueOf(game.get(i).getExperienceObtained()), dos);
                                                sendString(String.valueOf(game.get(i).getEnemiesEliminated()), dos);
                                                break;
                                            }

                                        }
                                        if (finished) {
                                            //if don't exist any game started, we create the game
                                            insert("game");
                                        }
                                        break;
                                    case "userBloodPoints":
                                        //reload users, send to the client blood points from the user selected
                                        System.out.println("get blodd points from "+user);
                                        users.clear();
                                        loadInfoDB("SELECT * FROM USERDATABASE", null, users, "user");
                                        String userBlood = receiveString(dis);
                                        for (int i = 0; i < users.size(); i++) {
                                            if (users.get(i).getName().equals(userBlood)) {
                                                sendString(String.valueOf(users.get(i).getBloodPoints()), dos);
                                                break;
                                            }
                                        }
                                        break;
                                    case "records":
                                        //reload users ordered for bloodpoints
                                        users.clear();
                                        loadInfoDB("SELECT * FROM USERDATABASE ORDER BY bloodPoints DESC", null, users, "user");
                                        //client send name user
                                        String userRecord = receiveString(dis);
                                        for (int i = 0; i < users.size(); i++) {
                                            System.out.println(users.get(i).getName());
                                            //server send to the client: position of the rakning and score from client
                                            if (users.get(i).getName().equals(userRecord)) {
                                                sendString(String.valueOf(i+1), dos);
                                                sendString(String.valueOf(users.get(i).getBloodPoints()), dos);
                                                break;
                                            }
                                        }
                                        //reload users ordered for bloodPoint,the first 5
                                        users.clear();
                                        loadInfoDB("SELECT * FROM USERDATABASE ORDER BY bloodPoints DESC LIMIT 5", null, users, "user");
                                        for (int i = 0; i < users.size(); i++) {
                                            System.out.println(users.get(i).getName());
                                            //server send to the client: position of the ranking and score from the first 5
                                            sendString(users.get(i).getName(), dos);
                                            sendString(String.valueOf(users.get(i).getBloodPoints()), dos);
                                        }
                                        //reload users
                                        users.clear();
                                        loadInfoDB("SELECT * FROM USERDATABASE", null, users, "user");
                                        break;
                                }
                                break;
                            case "update":
                                update(receiveString(dis));
                                break;
                            case "insert":
                                insert(receiveString(dis));
                                break;
                            default:
                                System.out.println("Only Login");
                                break;
                        }
                        sk.close();
                    } else {
                        //bad user
                        System.out.println("WrongUser");
                        sendString("WrongUser", dos);
                        //if client send register, we create user
                        String res = receiveString(dis);
                        if ("register".equals(res)) {
                            System.out.println("register");
                            insert("user");
                            sendString("createdUser", dos);
                        } else {
                            System.out.println("Closing connection");
                        }
                        sk.close();
                    }
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /*
    *function for initializing server
    */
    private void start() {
        try {
            //when start server we load basic info
            users = new ArrayList<UserDatabase>();
            charactersPlayables = new ArrayList<CharacterPlayable>();
            items = new ArrayList<Item>();
            inventory = new ArrayList<PlayerInventory>();
            game = new ArrayList<Game>();
            loadInfoDB("SELECT * FROM USERDATABASE", null, users, "user");
            loadInfoDB("SELECT * FROM ITEM", null, items, "items");
            loadInfoDB("SELECT * FROM GAME", null, game, "game");
            //start socket
            ServerSocket ssk = new ServerSocket(port);
            while (true) {
                //we accept any communication 
                Socket sk = ssk.accept();
                ServerThread server = new ServerThread(sk);
                server.start();
            }
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
    private static ResultSet query(String query, String searching) throws SQLException {
        Connection con = null;
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            con = connectDBSelect();
            if (searching == null) {
                //query without where
                statement = con.prepareStatement(query);
            } else {
                //query with where
                statement = con.prepareStatement(query);
                statement.setString(1, searching);

            }
            result = statement.executeQuery();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
    *function for load info to the classes
    *@param query
    *@param searching
    *@param data
    *@param classObject
    */
    private static void loadInfoDB(String query, String searching, ArrayList data, String classObject) {
        try {
            //array content data of query
            ArrayList<ArrayList<String>> dataQuery = getDataQuery(query, searching, classObject);
            for (int i = 0; i < dataQuery.size(); i++) {
                //pass data of array in the classes (creating objects) depending of the table
                switch (classObject) {
                    case "user":
                        data.add(new UserDatabase(dataQuery.get(i).get(0),
                                dataQuery.get(i).get(1), dataQuery.get(i).get(2),
                                dataQuery.get(i).get(3), Integer.parseInt(dataQuery.get(i).get(4))));
                        break;
                    case "character":
                        data.add(new CharacterPlayable(Integer.parseInt(dataQuery.get(i).get(0)),
                                Boolean.parseBoolean(dataQuery.get(i).get(1)), dataQuery.get(i).get(2),
                                Integer.parseInt(dataQuery.get(i).get(3)), Integer.parseInt(dataQuery.get(i).get(4)),
                                Integer.parseInt(dataQuery.get(i).get(5)), Integer.parseInt(dataQuery.get(i).get(6)),
                                Integer.parseInt(dataQuery.get(i).get(7)), Integer.parseInt(dataQuery.get(i).get(8)),
                                Integer.parseInt(dataQuery.get(i).get(9)), Integer.parseInt(dataQuery.get(i).get(10)),
                                dataQuery.get(i).get(11)));
                        break;
                    case "items":
                        data.add(new Item(Integer.parseInt(dataQuery.get(i).get(0)),
                                dataQuery.get(i).get(1),dataQuery.get(i).get(2), Boolean.parseBoolean(dataQuery.get(i).get(3)),
                                Integer.parseInt(dataQuery.get(i).get(4)),
                                Integer.parseInt(dataQuery.get(i).get(5))));
                        break;
                    case "inventory":
                        data.add(new PlayerInventory(Integer.parseInt(dataQuery.get(i).get(0)),
                                Integer.parseInt(dataQuery.get(i).get(1)),
                                Integer.parseInt(dataQuery.get(i).get(2))));
                        break;
                    case "game":
                        data.add(new Game(Integer.parseInt(dataQuery.get(i).get(0)),
                                Boolean.parseBoolean(dataQuery.get(i).get(1)),
                                Integer.parseInt(dataQuery.get(i).get(2)),
                                Integer.parseInt(dataQuery.get(i).get(3)),
                                Integer.parseInt(dataQuery.get(i).get(4)),
                                Integer.parseInt(dataQuery.get(i).get(5)),
                                Integer.parseInt(dataQuery.get(i).get(6)),
                                Integer.parseInt(dataQuery.get(i).get(7)),
                                Integer.parseInt(dataQuery.get(i).get(8))));
                        break;
                    case "gameID":
                        //get only game ID
                        idGame = Integer.parseInt(dataQuery.get(i).get(0));
                        break;
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*
    *function for pass query data to the array
    *@param query
    *@param searching
    *@param table
    *@return dataQuery
    */
    private static ArrayList<ArrayList<String>> getDataQuery(String query, String searching, String table) throws SQLException {
        ArrayList<ArrayList<String>> dataQuery = new ArrayList<ArrayList<String>>();
        ArrayList<String> fields = null;
        //get result of the query
        ResultSet result = query(query, searching);
        ResultSetMetaData resultMetaData = result.getMetaData();
        int fieldsCount = resultMetaData.getColumnCount();
        while (result.next()) {
            fields = new ArrayList<String>();
            //pass data of query in a array depending of the table
            switch (table) {
                case "user":
                    for (int i = 1; i < fieldsCount + 1; i++) {
                        fields.add(result.getString(i));
                    }
                    break;
                case "character":
                    fields.add(String.valueOf(result.getInt(1)));
                    fields.add(String.valueOf(result.getBoolean(2)));
                    fields.add(result.getString(3));
                    fields.add(String.valueOf(result.getInt(4)));
                    fields.add(String.valueOf(result.getInt(5)));
                    fields.add(String.valueOf(result.getInt(6)));
                    fields.add(String.valueOf(result.getInt(7)));
                    fields.add(String.valueOf(result.getInt(8)));
                    fields.add(String.valueOf(result.getInt(9)));
                    fields.add(String.valueOf(result.getInt(10)));
                    fields.add(String.valueOf(result.getInt(11)));
                    fields.add(result.getString(12));
                    break;
                case "items":
                    fields.add(String.valueOf(result.getInt(1)));
                    fields.add(result.getString(2));
                    fields.add(result.getString(3));
                    fields.add(String.valueOf(result.getBoolean(4)));
                    fields.add(String.valueOf(result.getInt(5)));
                    fields.add(String.valueOf(result.getInt(6)));
                    break;
                case "inventory":
                    fields.add(result.getString(1));
                    fields.add(result.getString(2));
                    fields.add(result.getString(3));
                    break;
                case "game":
                    fields.add(String.valueOf(result.getInt(1)));
                    fields.add(String.valueOf(result.getBoolean(2)));
                    fields.add(String.valueOf(result.getInt(3)));
                    fields.add(String.valueOf(result.getInt(4)));
                    fields.add(String.valueOf(result.getInt(5)));
                    fields.add(String.valueOf(result.getInt(6)));
                    fields.add(String.valueOf(result.getInt(7)));
                    fields.add(String.valueOf(result.getInt(8)));
                    fields.add(String.valueOf(result.getInt(9)));
                    break;
                case "gameID":
                    //get only game id
                    fields.add(String.valueOf(result.getInt(1)));
                    break;
            }
            dataQuery.add(fields);

        }
        return dataQuery;
    }

    /*
    *function to receive data from client (Unity)
    * @param is
    * @return received
    */
    private static String receiveString(InputStream is) {
        String received = "";
        try {
            byte[] lenBytes = new byte[4];
            is.read(lenBytes, 0, 4);
            int len = (((lenBytes[3] & 0xff) << 24) | ((lenBytes[2] & 0xff) << 16)
                    | ((lenBytes[1] & 0xff) << 8) | (lenBytes[0] & 0xff));
            byte[] receivedBytes = new byte[len];
            is.read(receivedBytes, 0, len);
            received = new String(receivedBytes, 0, len);

        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        return received;
    }

    /*
    *function to send data to the client (Unity)
    *@param toSend
    *@param os
    */
    private static void sendString(String toSend, OutputStream os) {
        try {
            byte[] toSendBytes = toSend.getBytes();
            int toSendLen = toSendBytes.length;
            byte[] toSendLenBytes = new byte[4];
            toSendLenBytes[0] = (byte) (toSendLen & 0xff);
            toSendLenBytes[1] = (byte) ((toSendLen >> 8) & 0xff);
            toSendLenBytes[2] = (byte) ((toSendLen >> 16) & 0xff);
            toSendLenBytes[3] = (byte) ((toSendLen >> 24) & 0xff);
            os.write(toSendLenBytes);
            os.write(toSendBytes);
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*
    *function to do insert in the database
    *@param table
    */
    private static void insert(String table) {
        Connection con = null;
        CallableStatement cstmt = null;
        try {
            con = connectDBonlyExecute();
            //do insert in the database depending of the table selected
            //data for the insert, receive from the client
            System.out.println("table selected: "+table);
            switch (table) {
                case "user":
                    cstmt = con.prepareCall("{call insertUser(?,?,?,?)}");
                    cstmt.setString(1, receiveString(dis));
                    cstmt.setString(2, receiveString(dis));
                    cstmt.setString(3, receiveString(dis));
                    cstmt.setString(4, receiveString(dis));
                    cstmt.execute();
                    users = new ArrayList<UserDatabase>();
                    loadInfoDB("SELECT * FROM USERDATABASE", null, users, "user");
                    break;
                case "character":
                    cstmt = con.prepareCall("{call insertCharacterplayable(?,?,?,?,?,?,?,?,?,?,?)}");
                    cstmt.setBoolean(1, true);
                    cstmt.setString(2, receiveString(dis));
                    cstmt.setInt(3, 1);
                    cstmt.setInt(4, Integer.parseInt(receiveString(dis)));
                    cstmt.setInt(5, Integer.parseInt(receiveString(dis)));
                    cstmt.setInt(6, Integer.parseInt(receiveString(dis)));
                    cstmt.setInt(7, Integer.parseInt(receiveString(dis)));
                    cstmt.setInt(8, 0);
                    cstmt.setInt(9, 20);
                    cstmt.setInt(10, 0);
                    cstmt.setString(11, receiveString(dis));
                    cstmt.execute();
                    charactersPlayables = new ArrayList<CharacterPlayable>();
                    loadInfoDB("SELECT * FROM CHARACTERPLAYABLE", null, charactersPlayables, "character");
                    break;
                case "game":
                    cstmt = con.prepareCall("{call insertGame(?,?,?,?,?,?,?,?)}");
                    cstmt.setBoolean(1, false);
                    cstmt.setInt(2, 1);
                    cstmt.setInt(3, 0);
                    cstmt.setInt(4, 0);
                    cstmt.setInt(5, 1);
                    cstmt.setInt(6, 0);
                    cstmt.setInt(7, 0);
                    System.out.println(playerID);
                    cstmt.setInt(8, playerID);
                    cstmt.execute();
                    break;
            }

        } catch (SQLException ex) {
            if (ex instanceof SQLIntegrityConstraintViolationException) {
                //if try insert existing primarey key informs client
                sendString("ExistingPrimaryKey", dos);
            } else {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    /*
    *function to do update in the database
    *@param table
    */
    private static void update(String table) {
        Connection con = null;
        CallableStatement cstmt = null;
        try {
            con = connectDBonlyExecute();
            System.out.println("table selected: "+table);
            switch (table) {
                case "user":
                    //update bloodpoints from user in database
                    cstmt = con.prepareCall("{call updateUser(?,?)}");
                    cstmt.setString(1, receiveString(dis));
                    int blood= Integer.parseInt(receiveString(dis));
                    System.out.println("blood:" + blood);
                    cstmt.setInt(2,blood );
                    cstmt.executeUpdate();
                    //update data from users in classes
                    users.clear();
                    loadInfoDB("SELECT * FROM USERDATABASE", null, users, "user");
                    break;
                case "game":
                    //update game in database
                    cstmt = con.prepareCall("{call updateGame(?,?,?,?,?,?,?,?)}");
                    idGame = Integer.parseInt(receiveString(dis));
                    cstmt.setInt(1, idGame);
                    cstmt.setBoolean(2, Boolean.parseBoolean(receiveString(dis)));
                    cstmt.setInt(3, Integer.parseInt(receiveString(dis)));
                    cstmt.setInt(4, Integer.parseInt(receiveString(dis)));
                    cstmt.setInt(5, Integer.parseInt(receiveString(dis)));
                    cstmt.setInt(6, Integer.parseInt(receiveString(dis)));
                    cstmt.setInt(7, Integer.parseInt(receiveString(dis)));
                    cstmt.setInt(8, Integer.parseInt(receiveString(dis)));
                    cstmt.executeUpdate();
                    //update data from game in classes
                    game.clear();
                    loadInfoDB("SELECT * FROM GAME", null, game, "game");
                    break;
                case "itemInventory":
                    //update items inventory from character in database
                    System.out.println("update items");
                    cstmt = con.prepareCall("{call updateItemInventory(?,?,?)}");
                    int idCharacter = Integer.parseInt(receiveString(dis));
                    cstmt.setInt(1, idCharacter);
                    cstmt.setInt(2, Integer.parseInt(receiveString(dis)));
                    cstmt.setInt(3, Integer.parseInt(receiveString(dis)));
                    cstmt.executeUpdate();
                    //update data from player inventory in classes
                    inventory.clear();
                    loadInfoDB("SELECT * FROM PLAYERINVENTORY WHERE characterPlayableID = ?", String.valueOf(idCharacter), inventory, "inventory");
                    break;
                case "character":
                    //update data from character in database
                    cstmt = con.prepareCall("{call updateCharacterplayable(?,?,?,?,?,?,?,?,?,?)}");
                    cstmt.setInt(1, Integer.parseInt(receiveString(dis)));
                    cstmt.setBoolean(2, Boolean.parseBoolean(receiveString(dis)));
                    cstmt.setInt(3, Integer.parseInt(receiveString(dis)));
                    cstmt.setInt(4, Integer.parseInt(receiveString(dis)));
                    cstmt.setInt(5, Integer.parseInt(receiveString(dis)));
                    cstmt.setInt(6, Integer.parseInt(receiveString(dis)));
                    cstmt.setInt(7, Integer.parseInt(receiveString(dis)));
                    cstmt.setInt(8, Integer.parseInt(receiveString(dis)));
                    cstmt.setInt(9, Integer.parseInt(receiveString(dis)));
                    cstmt.setInt(10, Integer.parseInt(receiveString(dis)));
                    cstmt.executeUpdate();
                    //update data from character in classes
                    charactersPlayables = new ArrayList<CharacterPlayable>();
                    loadInfoDB("SELECT * FROM CHARACTERPLAYABLE", null, charactersPlayables, "character");
                    break;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*
    *function for connect to the database with the permission "only select"
    *@return con
    */
    private static Connection connectDBSelect() throws SQLException {
        Connection con
                = DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/ROGUEKNIGHT?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "RogueReader", "ReadData");
        return con;
    }

    /*
    *function for connect to the database with the permission "only execute"
    *@return con
    */
    private static Connection connectDBonlyExecute() throws SQLException {
        Connection con
                = DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/ROGUEKNIGHT?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "RogueExec", "ExecQueries");
        return con;
    }
}

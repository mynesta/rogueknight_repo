/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

/**
 *
 * @author manu and sergio
 */
public class UserDatabase {
    //variables
    private String username;
    private String email;
    private String birthDate;
    private String password;
    private int bloodPoints;
    

    /**
     * constructor class
     * @param username
     * @param email
     * @param birthDate
     * @param password
     * @param bloodPoints
     */

    public UserDatabase(String username,String email,String birthDate,String password,int bloodPoints) {
        this.username = username;
        this.email = email;
        this.birthDate = birthDate;
        this.password = password;
        this.bloodPoints = bloodPoints;
    }

    /**
     *getPassword
     * @return password
     */

    public String getPassword() {
        return password;
    }

    /**
     *getName
     * @return username
     */
    public String getName() {
        return username;
    }

    /**
     *getEmail
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     *getBirthDate
     * @return birthDate
     */
    public String getBirthDate() {
        return birthDate;
    }

    /**
     *getBloodPoints
     * @return bloodPoints
     */
    public int getBloodPoints() {
        return bloodPoints;
    }

}

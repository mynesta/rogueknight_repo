﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovingObject : MonoBehaviour
{
    //variables
    public float moveTime = 0.01f;
    public LayerMask blockingLayer;

    private BoxCollider2D boxCollider;
    private Rigidbody2D rb2D;
    private float inverseMoveTime;

    /// <summary>
    ///start moving object
    /// </summary>
    protected virtual void Start()
    {
        //get components
        boxCollider = GetComponent<BoxCollider2D>();
        rb2D = GetComponent<Rigidbody2D>();
        inverseMoveTime = 1f / moveTime;
    }

    /// <summary>
    ///function to move
    /// </summary>
    /// <param name="hit"></param>
    /// <param name="xDir"></param>
    /// <param name="yDir"></param>
    protected bool Move (int xDir, int yDir, out RaycastHit2D hit)
    {
        // get position actual and move to destiny
        Vector2 start = transform.position;
        Vector2 end = start + new Vector2(xDir,yDir);
        boxCollider.enabled = false;
        hit = Physics2D.Linecast(start, end, blockingLayer);
        boxCollider.enabled = true;
        //if you do not aim at any object you move
        if (hit.transform == null)
        {
            StartCoroutine(SmoothMovement(end));
            return true;
        }
        return false;
    }

    /// <summary>
    ///coroutine to move 
    /// </summary>
    /// <param name="end"></param>
    protected IEnumerator SmoothMovement(Vector3 end)
    {
        float sqrRemainingDistance = (transform.position - end).sqrMagnitude;
        while (sqrRemainingDistance > float.Epsilon)
        {
            Vector3 newPosition = Vector3.MoveTowards(rb2D.position, end, inverseMoveTime * Time.deltaTime);
            rb2D.MovePosition(newPosition);
            sqrRemainingDistance = (transform.position - end).sqrMagnitude;
            yield return null;
        }
    }

    /// <summary>
    ///function to attemptmove
    /// </summary>
    /// <param name="yDir"></param>
    /// <param name="xDir"></param>
    protected virtual void AttemptMove <T> (int xDir, int yDir) where T: Component
    {
        RaycastHit2D hit;
        bool canMove = Move(xDir, yDir,out hit);
        if(hit.transform == null)
        {
            return;
        }
        //get components objects
        T hitComponent = hit.transform.GetComponent<T>();
        Enemy pointingEnemy = hit.transform.GetComponent<Enemy>();
        Player player = GetComponent<Player>();
        Enemy enemy = GetComponent<Enemy>();
        Wall pointingWall = hit.transform.GetComponent<Wall>();
        //player attack wall or enemy attack player
        if (!canMove && hitComponent != null && pointingEnemy == null)
        {
            OnCantMove(hitComponent);
        }
        //enemy attack wall
        if (!canMove && enemy != null && pointingWall != null)
        {
            EnemyAttackWall(pointingWall);
        }
        //player attack enemy
        if (!canMove && player != null && pointingEnemy != null)
        {
            PlayerAttackEnemy(pointingEnemy);
        }  
    }

    protected abstract void OnCantMove<T>(T component) where T : Component;

    protected abstract void PlayerAttackEnemy<T>(T component) where T : Component;

    protected abstract void EnemyAttackWall<T>(T component) where T : Component;

}

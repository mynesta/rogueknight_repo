﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CreateCharacter : MonoBehaviour
{
    //variables
    public InputField characterName;
    private string user;
    private string password;

    private string url = "mysqlantimateria.ddns.net";
    private int port = 12345;

    private int hitPoints;
    private int attack;
    private int defense;
    private int inventory;
    private int scene;

    private string message;

    public Text bloodPointsCurrent;
    public Text messageLog;

    /// <summary>
    ///start player
    /// </summary>
    void Start()
    {
        //get user
        user = Login.user;
        password = Login.pass;
        //load blood points from user
        Thread threadBloodPoints = new Thread(new ThreadStart(bloodPoints));
        threadBloodPoints.Start();
        threadBloodPoints.Join();
        // load text blood points
        bloodPointsCurrent.text = message;
    }

    /// <summary>
    ///call create character and change scene to main menu
    /// </summary>
    public void callCreateChacracter()
    {
        //need fill fields
        if (characterName.text != "")
        {
            randoms();
            Thread threadCreateCharacter = new Thread(new ThreadStart(createCharacter));
            threadCreateCharacter.Start();
            threadCreateCharacter.Join();
            scene = SceneManager.GetActiveScene().buildIndex - 1;
            SceneManager.LoadScene(scene);
        }
        else
        {
            messageLog.text = "Missing fields to fill";
            StartCoroutine(ShowMessage(2));
        }

    }

    /// <summary>
    ///generate data new character randomly
    /// </summary>
    private void randoms()
    {
        hitPoints = UnityEngine.Random.Range(8, 15);
        attack = UnityEngine.Random.Range(4, 10);
        defense = UnityEngine.Random.Range(2, 5);
        inventory = UnityEngine.Random.Range(5, 10);
    }

    /// <summary>
    ///coroutine to show mesage for a few seconds
    /// </summary>
    /// <param name="delay"></param>
    IEnumerator ShowMessage(float delay)
    {
        yield return new WaitForSeconds(delay);
        messageLog.text = "";
    }

    /// <summary>
    ///thread to get blood points from user
    /// </summary>
    private void bloodPoints()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("select", clientSocket);
            sendString("userBloodPoints", clientSocket);
            //get blood points and show
            sendString(user, clientSocket);
            message = (receiveString(clientSocket));
        }
    }

    /// <summary>
    ///thread to create new character
    /// </summary>
    private void createCharacter()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("insert", clientSocket);
            sendString("character", clientSocket);
            //send data for insert
            sendString(characterName.text, clientSocket);
            sendString(Convert.ToString(hitPoints), clientSocket);
            sendString(Convert.ToString(attack), clientSocket);
            sendString(Convert.ToString(defense), clientSocket);
            sendString(Convert.ToString(inventory), clientSocket);
            sendString(user, clientSocket);
        }
    }

    /// <summary>
    ///function for receive data from server
    /// </summary>
    /// <param name="clientSocket"></param>
    /// <returns>rcv</returns>
    private String receiveString(Socket clientSocket)
    {
        byte[] rcvLenBytes = new byte[4];
        clientSocket.Receive(rcvLenBytes);
        int rcvLen = System.BitConverter.ToInt32(rcvLenBytes, 0);
        byte[] rcvBytes = new byte[rcvLen];
        clientSocket.Receive(rcvBytes);
        String rcv = System.Text.Encoding.ASCII.GetString(rcvBytes);
        return rcv;
    }

    /// <summary>
    ///function for send data to server
    /// </summary>
    /// <param name="toSend"></param>
    /// <param name="clientSocket"></param>
    private void sendString(String toSend, Socket clientSocket)
    {
        int toSendLen = System.Text.Encoding.ASCII.GetByteCount(toSend);
        byte[] toSendBytes = System.Text.Encoding.ASCII.GetBytes(toSend);
        byte[] toSendLenBytes = System.BitConverter.GetBytes(toSendLen);
        clientSocket.Send(toSendLenBytes);
        clientSocket.Send(toSendBytes);
    }

}

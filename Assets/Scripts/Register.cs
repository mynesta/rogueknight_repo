﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Register : MonoBehaviour
{
    //variables
    public InputField password;
    public InputField repeatPassword;
    public InputField email;
    public InputField birthdate;
    public InputField username;
    public Text messageLog;
    private string message;
    private int scene;

    private string url = "mysqlantimateria.ddns.net";
    private int port = 12345;

    /// <summary>
    /// functio to create new user
    /// </summary>
    public void createUser()
    {
        messageLog.text = "";
        //fill fields
        if(email.text != "" && username.text != "" && birthdate.text != "" && password.text != "" && repeatPassword.text != "")
        {
            //password correct and repeat password correct
            if (password.text == repeatPassword.text)
            {
                Thread register = new Thread(new ThreadStart(threadRegisterUser));
                register.Start();
                register.Join();
            }
            else
            {
                message = "Password doesn't match";
            }
        }
        else
        {
            message = "Missing fields to fill";
        }
        

        messageLog.text = message;
        StartCoroutine(ShowMessage(2));


    }

    /// <summary>
    /// coroutine to show message for a few seconds
    /// </summary>
    /// <param name="delay"></param>
    IEnumerator ShowMessage(float delay)
    {
        yield return new WaitForSeconds(delay);
        messageLog.text = "";
    }

    /// <summary>
    /// thread to register user (create user)
    /// </summary>
    private void threadRegisterUser()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(username.text, clientSocket);
        sendString(password.text, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            sendString("login", clientSocket);
            message = "User already exists";
        }
        else
        {
            // register user if doesn't exist
            sendString("register", clientSocket);
            sendString(username.text, clientSocket);
            sendString(email.text, clientSocket);
            sendString(birthdate.text, clientSocket);
            sendString(password.text, clientSocket);
            string result = receiveString(clientSocket);

            if (result == "ExistingPrimaryKey")
            {
                message = "User already exists";

            }
            else if (result == "createdUser")
            {
                message = "User successfully created";
            }
        }



    }

    /// <summary>
    ///function for receive data from server
    /// </summary>
    /// <param name="clientSocket"></param>
    /// <returns>rcv</returns>
    private String receiveString(Socket clientSocket)
    {
        byte[] rcvLenBytes = new byte[4];
        clientSocket.Receive(rcvLenBytes);
        int rcvLen = System.BitConverter.ToInt32(rcvLenBytes, 0);
        byte[] rcvBytes = new byte[rcvLen];
        clientSocket.Receive(rcvBytes);
        String rcv = System.Text.Encoding.ASCII.GetString(rcvBytes);
        return rcv;
    }

    /// <summary>
    ///function for send data to server
    /// </summary>
    /// <param name="toSend"></param>
    /// <param name="clientSocket"></param>
    private void sendString(String toSend, Socket clientSocket)
    {
        int toSendLen = System.Text.Encoding.ASCII.GetByteCount(toSend);
        byte[] toSendBytes = System.Text.Encoding.ASCII.GetBytes(toSend);
        byte[] toSendLenBytes = System.BitConverter.GetBytes(toSendLen);
        clientSocket.Send(toSendLenBytes);
        clientSocket.Send(toSendBytes);
    }

    /// <summary>
    ///function to back to the login screen
    /// </summary>
    public void back()
    {
        scene = SceneManager.GetActiveScene().buildIndex - 1;
        SceneManager.LoadScene(scene);
    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Web;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MovingObject
{
    //variables
    public int wallDamage = 1;
    public int pointsPerFood = 10;
    public int pointsPerSoda = 20;
    public float restartLevelDelay = 1f;
    private Text lifeText;
    private Text attackText;
    private Text defenseText;
    public static Text experienceText;
    private Text goldText;
    private Text levelText;
    private Text nicknameText;

    public GameObject[] useArmors;
    public GameObject[] useWeapons;

    public GameObject[] armors;
    public GameObject[] weapons;

    public AudioClip moveSound1;
    public AudioClip moveSound2;
    public AudioClip playerChop1;
    public AudioClip playerChop2;
    public AudioClip eatSound1;
    public AudioClip eatSound2;
    public AudioClip drinkSound1;
    public AudioClip drinkSound2;
    public AudioClip gameOverSound;

    private Animator animator;
    public static int life;
    public static int maxLife;
    private SpriteRenderer look;
    public static int attack;
    public static int defense;
    private int attackFinal;
    private int defenseFinal;
    public static int inventorySlotsMax;
    private string nickname;
    public static int experience;
    public static int gold;
    public static int maxExperience;
    public static int levelPlayer;
    private int quantityGold;
    private GameObject cam;
    private Transform inventoryUI;
    public List<InventoryItem> items = new List<InventoryItem>();
    public GameObject item;
    private int inventorySlotsFill;
    private Button moveUp;
    private Button moveDown;
    private Button moveLeft;
    private Button moveRight;


    /// <summary>
    ///start player
    /// </summary>
    protected override void Start()
    {
        inventorySlotsFill = 0;
        look = gameObject.GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        //load inventory 
        inventoryUI = GameObject.Find("InventoryLayout").GetComponent<Transform>();
        //load texts
        lifeText = GameObject.Find("LifeText").GetComponent<Text>();
        attackText = GameObject.Find("AttackText").GetComponent<Text>();
        defenseText = GameObject.Find("DefenseText").GetComponent<Text>();
        experienceText = GameObject.Find("ExperienceText").GetComponent<Text>();
        goldText = GameObject.Find("GoldText").GetComponent<Text>();
        levelText = GameObject.Find("LevelTextPlayer").GetComponent<Text>();
        nicknameText = GameObject.Find("NicknameText").GetComponent<Text>();
        //load buttons
        moveUp = GameObject.Find("MoveUp").GetComponent<Button>();
        moveDown = GameObject.Find("MoveDown").GetComponent<Button>();
        moveLeft = GameObject.Find("MoveLeft").GetComponent<Button>();
        moveRight = GameObject.Find("MoveRight").GetComponent<Button>();
        //load camera
        cam = GameObject.Find("Main Camera");
        //load data player
        maxLife = GameManager.instance.maxPlayerLifePoints;
        life = GameManager.instance.playerLifePoints;
        attackFinal = GameManager.instance.playerAttackTotal;
        defenseFinal = GameManager.instance.playerDefenseTotal;
        attack = GameManager.instance.playerAttack;
        if (attackFinal == 0)
        {
            attackFinal = attack;
        }
        defense = GameManager.instance.playerDefense;
      
        if (defenseFinal == 0)
        {
            defenseFinal = defense;
        }
        levelPlayer = GameManager.instance.playerLevel;
        inventorySlotsMax = GameManager.instance.inventorySlots;
        nickname = GameManager.instance.nickname;
        experience = GameManager.instance.experience;
        gold = GameManager.instance.gold;
        maxExperience = GameManager.instance.needExperience;
        items = GameManager.instance.items;
        deleteAllItemsUI();
        loadItemsUI();
        //update texts
        lifeText.text = "Life: " + life + "/" + maxLife;
        attackText.text = "Attack: " + attackFinal;
        defenseText.text = "Defense: " + defenseFinal;
        levelText.text = "Level: " + levelPlayer;
        nicknameText.text = nickname;
        goldText.text = "Gold: " + gold;
        experienceText.text = "exp: " + experience + "/" + maxExperience;
        base.Start();
        //update camera,follow player
        cam.transform.position = new Vector3((transform.position.x)+2, (transform.position.y), -10);
        cam.transform.SetParent(transform);
    }

    /// <summary>
    ///load items in UI
    /// </summary>
    public void loadItemsUI()
    {
        //to count items
        inventorySlotsFill = 0;
        for (int i = 0;i<items.Count;i++)
        {
            for (int l = 0; l < (items[i].quantity); l++)
            {
                inventorySlotsFill += 1;
                //load image item
                item.GetComponent<Image>().sprite = items[i].sprite;
                //instanciate item
                GameObject instantiated = Instantiate(item, inventoryUI);
                instantiated.AddComponent<Button>();
                //generate button to the item and add function for use item
                switch(items[i].idItem){
                    case 1:
                        instantiated.GetComponent<Button>().onClick.AddListener(useItemLeatherArmor);
                        break;
                    case 2:
                        instantiated.GetComponent<Button>().onClick.AddListener(useItemPlateMail);
                        break;
                    case 3:
                        instantiated.GetComponent<Button>().onClick.AddListener(useItemPotion);
                        break;
                    case 4:
                        instantiated.GetComponent<Button>().onClick.AddListener(useItemQualityPotion);
                        break;
                    case 5:
                        instantiated.GetComponent<Button>().onClick.AddListener(useItemHeroicPotion);
                        break;
                    case 6:
                        instantiated.GetComponent<Button>().onClick.AddListener(useItemDivinePotion);
                        break;
                    case 7:
                        instantiated.GetComponent<Button>().onClick.AddListener(useItemBroadsword);
                        break;
                    case 8:
                        instantiated.GetComponent<Button>().onClick.AddListener(useItemWarAxe);
                        break;

                }
                
            }
                
            
        }
        
    }

    /// <summary>
    ///call function to use leather armor
    /// </summary>
    void useItemLeatherArmor()
    {
        useItemInventory(1, 0);
    }

    /// <summary>
    ///call function to use plate mail
    /// </summary>
    void useItemPlateMail()
    {
        useItemInventory(2, 0);
    }

    /// <summary>
    ///call function to use potion
    /// </summary>
    void useItemPotion()
    {
        useItemInventory(3, 5);
    }

    /// <summary>
    ///call function to use quality potion
    /// </summary>
    void useItemQualityPotion()
    {
        useItemInventory(4, 15);
    }

    /// <summary>
    ///call function to use heroic potion
    /// </summary>
    void useItemHeroicPotion()
    {
        useItemInventory(5, 40);
    }

    /// <summary>
    ///call function to use Divine potion
    /// </summary>
    void useItemDivinePotion()
    {
        useItemInventory(6, 100);
    }

    /// <summary>
    ///call function to use broadsword
    /// </summary>
    void useItemBroadsword()
    {
        useItemInventory(7, 0);
    }

    /// <summary>
    ///call function to use war axe
    /// </summary>
    void useItemWarAxe()
    {
        useItemInventory(8, 0);
    }

    /// <summary>
    ///call function to delete items from inventory (UI)
    /// </summary>
    public void deleteAllItemsUI()
    {
        
        for(int i = 0; i < inventoryUI.childCount; i++)
        {
            Destroy(inventoryUI.GetChild(i).gameObject);
        }
    }

    /// <summary>
    /// function to save data with GameManager
    /// </summary>
    private void OnDisable()
    {
        GameManager.instance.playerLifePoints = life;
        GameManager.instance.maxPlayerLifePoints = maxLife;
        GameManager.instance.experience = experience;
        GameManager.instance.needExperience = maxExperience;
        GameManager.instance.gold = gold;
        GameManager.instance.playerAttack = attack;
        GameManager.instance.playerDefense = defense;
        GameManager.instance.playerLevel = levelPlayer;
        GameManager.instance.items = items;
        GameManager.instance.playerAttackTotal = attackFinal;
        GameManager.instance.playerDefenseTotal = defenseFinal;
    }

    /// <summary>
    ///function to move player
    /// </summary>
    /// <param name="xDir"></param>
    /// <param name="yDir"></param>
    protected override void AttemptMove<T>(int xDir, int yDir)
    {
        SoundManager.instance.RandomizeSfx(moveSound1, moveSound2);
        lifeText.text = "Life: " + life + "/" + maxLife;
        base.AttemptMove<T>(xDir, yDir);
        CheckIfGameOver();
        //change turn
        GameManager.instance.playersTurn = false;
        
    }

    /// <summary>
    ///function detect colisions with player
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter2D(Collider2D other)
    {
        //change level
        if (other.tag == "Exit")
        {
            Invoke("Restart", restartLevelDelay);
            enabled = false;
            GameManager.firstFloor = true;
        }
        //get any potion
        else if(other.tag == "Potion")
        {
            //get option if you do not have full inventory
            if (inventorySlotsFill < inventorySlotsMax)
            {
                //load image item
                item.GetComponent<Image>().sprite = other.gameObject.GetComponent<SpriteRenderer>().sprite;
                bool exist = false;
                //add item quantity if we have miminum 1 (list)
                for (int i = 0; i < items.Count; i++)
                {
                    Debug.Log(items[i].idItem);
                    Debug.Log(other.gameObject.GetComponent<Item>().id);
                    if (items[i].idItem == other.gameObject.GetComponent<Item>().id)
                    {
                        exist = true;
                        if ((!items[i].equip) || (items[i].equip && items[i].quantity < 1))
                        {
                            Debug.Log("repetido");
                            items[i].quantity = (items[i].quantity + 1);
                            Debug.Log("quantity: " + items[i].quantity);
                            break;
                        }
                        
                    }
                }
                //if dont have minimum 1 add new object (list)
                if (!exist)
                {
                    Debug.Log(other.gameObject.GetComponent<SpriteRenderer>().sprite.name);
                    InventoryItem inventoryItem = new InventoryItem(other.gameObject.GetComponent<Item>().nameItem,
                        other.gameObject.GetComponent<Item>().id, item.GetComponent<Image>().sprite, 1,
                        other.gameObject.GetComponent<Item>().equip, other.gameObject.GetComponent<Item>().sellCost, other.gameObject.GetComponent<Item>().buyCost);
                    items.Add(inventoryItem);
                }
                GameManager.instance.gameItemsObtained += 1;
                other.gameObject.SetActive(false);
                //reload items UI
                deleteAllItemsUI();
                loadItemsUI();
            }

        }
        //if we step a harmfulobject ,receive damage
        else if(other.tag == "harmfulObject")
        {
            life -= 2;
            lifeText.text = "Life: " + life + "/" + maxLife;
            CheckIfGameOver();
            animator.SetTrigger("playerHit");
        }
        // get gold
        else if(other.tag == "Gold")
        {
            gold += other.gameObject.GetComponent<Gold>().quantity;
            GameManager.instance.gameGoldObtained += other.gameObject.GetComponent<Gold>().quantity; ;
            goldText.text = "Gold: " + gold;
            other.gameObject.SetActive(false);
        }

    }

    /// <summary>
    ///function potion effect
    /// </summary>
    /// <param name="healing"></param>
    private void potionEffect(int healing)
    {
        if (life < maxLife && (maxLife - life) < healing)
        {
            life = maxLife;
        }
        else if (life < maxLife && (maxLife - life) > healing)
        {
            life += healing;
        }
        lifeText.text = "Life: " + life + "/" + maxLife;
        SoundManager.instance.RandomizeSfx(drinkSound1, drinkSound2);
    }

    /// <summary>
    ///function to use items inventory
    /// </summary>
    /// <param name="healing"></param>
    /// <param name="id"></param>
    private void useItemInventory(int id,int healing)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].idItem == id)
            {
                //use items if we have minimum 1
                if (items[i].quantity > 0)
                {
                    if (items[i].equip)
                    {
                        //armors
                        if (items[i].idItem < 3)
                        {
                            if(!useArmors[(items[i].idItem) - 1].GetComponent<Item>().usingEquipable)
                            {
                                for (int l = 0; l < items.Count; l++)
                                {
                                    //activated armor
                                    if(items[l].idItem < 3)
                                    {
                                        items[l].sprite = armors[(items[l].idItem) - 1].GetComponent<SpriteRenderer>().sprite;
                                        useArmors[(items[l].idItem) - 1].GetComponent<Item>().usingEquipable = false;
                                    }
                                }
                                //aplicated buff armor
                                if(items[i].idItem == 1)
                                {

                                    defenseFinal = ((25 * defense) / 100)+defense;
                                }
                                else
                                {
                                    defenseFinal = ((40 * defense) / 100)+defense;
                                }
                                //change sprite for use item
                                useArmors[(items[i].idItem) - 1].GetComponent<Item>().usingEquipable = true;
                                items[i].sprite = useArmors[(items[i].idItem) - 1].GetComponent<SpriteRenderer>().sprite;
                            }
                            //deactivate item
                            else
                            {
                                useArmors[(items[i].idItem) - 1].GetComponent<Item>().usingEquipable = false;
                                items[i].sprite = armors[(items[i].idItem) - 1].GetComponent<SpriteRenderer>().sprite;
                                defenseFinal = defense;
                            }
                            defenseText.text = "Defense: " + defenseFinal;

                        }
                        //weapons
                        else if(items[i].idItem > 6)
                        {
                            if (!useWeapons[(items[i].idItem) - 7].GetComponent<Item>().usingEquipable)
                            {
                                for (int l = 0; l < items.Count; l++)
                                {
                                    //activated weapon
                                    if (items[l].idItem > 6)
                                    {
                                        items[l].sprite = weapons[(items[l].idItem) - 7].GetComponent<SpriteRenderer>().sprite;
                                        useWeapons[(items[l].idItem) - 7].GetComponent<Item>().usingEquipable = false;
                                    }
                                }
                                //aplicated buff weapon
                                if (items[i].idItem == 7)
                                {

                                    attackFinal = ((25 * attack) / 100)+attack;
                                }
                                else
                                {
                                    attackFinal = ((40 * attack) / 100)+attack;
                                }
                                //change sprite for use item
                                useWeapons[(items[i].idItem) - 7].GetComponent<Item>().usingEquipable = true;
                                items[i].sprite = useWeapons[(items[i].idItem) - 7].GetComponent<SpriteRenderer>().sprite;
                            }
                            //deactivate item
                            else
                            {
                                useWeapons[(items[i].idItem) - 7].GetComponent<Item>().usingEquipable = false;
                                items[i].sprite = weapons[(items[i].idItem) - 7].GetComponent<SpriteRenderer>().sprite;
                                attackFinal = attack;
                            }
                            attackText.text = "Attack: " + attackFinal;
                        }
                    }
                    //use potion,we delete used potion and healing
                    else
                    {
                        Debug.Log("repetido");
                        items[i].quantity -= 1;
                        Debug.Log("quantity: " + items[i].quantity);
                        potionEffect(healing);
                    }
                    break;
                }


            }
        }
        //update items UI
        deleteAllItemsUI();
        loadItemsUI();
    }

    /// <summary>
    ///function to attack wall
    /// </summary>
    /// <param name="component"></param>
    protected override void OnCantMove<T>(T component)
    {
        Wall hitWall = component as Wall;
        hitWall.DamageWall(wallDamage);
        animator.SetTrigger("playerChop");
    }

    /// <summary>
    ///function to attack enemy
    /// </summary>
    /// <param name="component"></param>
    protected override void PlayerAttackEnemy<T>(T component) {
        Enemy hitEnemy = component as Enemy;
        animator.SetTrigger("playerChop");
        //enemy receive damage
        hitEnemy.LoseLife(hitEnemy.combatSystem(attackFinal,hitEnemy.defense));
        if (!hitEnemy.alive)
        {
            GameManager.instance.gameEnemiesEliminated += 1;
            if (experience < maxExperience)
            {
                //gain exp
                GameManager.instance.gameExperienceObtained += hitEnemy.defense;
                experience += hitEnemy.defense;
                //level up player
                if (experience >= maxExperience)
                {
                    experience = 0;
                    maxExperience += (maxExperience/2);
                    levelPlayer += 1;
                    levelText.text = "level: " + levelPlayer;
                    int lifeIncrement = UnityEngine.Random.Range(1, 10);
                    int attackIncrement = UnityEngine.Random.Range(1, 5);
                    int defenseIncrement = UnityEngine.Random.Range(1, 3);
                    life += lifeIncrement;
                    maxLife += lifeIncrement;
                    attack += attackIncrement;
                    attackFinal += attackIncrement;
                    defense += defenseIncrement;
                    defenseFinal += defenseIncrement;
                    lifeText.text = "life: " + life + "/" + maxLife;
                    attackText.text = "attack: " + attackFinal;
                    defenseText.text = "defense: " + defenseFinal;
                }
            }
            experienceText.text = "exp: " + experience+"/"+maxExperience;
        }
    }

    protected override void EnemyAttackWall<T>(T component) { }

    /// <summary>
    ///function to restart (next level)
    /// </summary>
    void Restart()
    {
        GameManager.instance.calculateBloodPoints();
        SceneManager.LoadScene(4);
    }

    /// <summary>
    ///function to receive damage
    /// </summary>
    /// <param name="loss"></param>
    public void LoseLife(int loss)
    {
        animator.SetTrigger("playerHit");
        life -= loss;
        lifeText.text = "- " + loss + " Life: " + life + "/" + maxLife;
        CheckIfGameOver();
    }

    /// <summary>
    ///function frame per frame
    /// </summary>
    void Update()
    {
        if (!GameManager.instance.playersTurn)
        {
            return;
        }
        int horizontal = 0;
        int vertical = 0;
        //controls for pc and web (arrows)
#if UNITY_STANDALONE || UNITY_WEBPLAYER
        
        horizontal = (int)(Input.GetAxisRaw("Horizontal"));
        vertical = (int)(Input.GetAxisRaw("Vertical"));

        
#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
        //controls mobile (click screen with buttons)
        if (moveUp.GetComponent<ButtonsMove>().activated)
        {
            vertical = 1;
            moveUp.GetComponent<ButtonsMove>().activated = false;
        }
        else if (moveDown.GetComponent<ButtonsMove>().activated)
        {
            vertical = -1;
            moveDown.GetComponent<ButtonsMove>().activated = false;
        }
        else if (moveLeft.GetComponent<ButtonsMove>().activated)
        {
            horizontal = -1;
            moveLeft.GetComponent<ButtonsMove>().activated = false;
        }
        else if (moveRight.GetComponent<ButtonsMove>().activated)
        {
            horizontal = 1;
            moveRight.GetComponent<ButtonsMove>().activated = false;
        }


#endif
        //move player
        if (horizontal == -1)
        {
            look.flipX = true;
        }
        else
        {
            look.flipX = false;
        }
        if (horizontal != 0)
        {
            vertical = 0;
        }
        if (horizontal != 0 || vertical != 0)
        {
             AttemptMove<Wall>(horizontal, vertical);
        }

    }

    /// <summary>
    ///function to check if the player is dies
    /// </summary>
    private void CheckIfGameOver()
    {
        if (life < 1)
        {
            SoundManager.instance.PlaySingle(gameOverSound);
            SoundManager.instance.musicSource.Stop();
            GameManager.instance.GameOver();
        }
    }

}

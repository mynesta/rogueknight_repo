﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour
{

    [Serializable]
    public class Count
    {
        //variables
        public int minimum;
        public int maximum;

        /// <summary>
        /// function to genereate minimum and maximum for something
        /// </summary>
        /// <param name="maximum"></param>
        /// <param name="minimum"></param>
        public Count(int minimum,int maximum)
        {
            this.minimum = minimum;
            this.maximum = maximum;
        }
    }
   
    //hideininspector prevents it from coming out in the inspector
    [HideInInspector] public int wallCountMin;
    [HideInInspector] public int wallCountMax;
    [HideInInspector] public int foodCountMin;
    [HideInInspector] public int foodCountMax;
    [HideInInspector] public int harmfulObjetcsCountMin;
    [HideInInspector] public int harmfulObjetcsCountMax;
    [HideInInspector] public int enemiesCountMin;
    [HideInInspector] public int enemiesCountMax;
    [HideInInspector] public int goldCountMin;
    [HideInInspector] public int goldCountMax;

    //variables
    public int columnsBoard;
    public int rowsBoard;
    private int columns;
    private int rows;
    public int outerWall;

    Count wallCount;
    Count foodCount;
    Count harmfulObjetcsCount;
    Count enemiesCount;
    Count goldCount;


    public GameObject gold;
    public GameObject exit;
    public GameObject[] floorTiles; 
    public GameObject[] wallTiles;
    public GameObject[] itemsTiles;
    public GameObject[] onlyPotionsTiles;
    public GameObject[] enemyTiles;
    public GameObject[] outerWallTiles;
    public GameObject[] harmfulObjects;

    public GameObject player;
    
    private Transform boardHolder;
    private List<Vector3> gridPositions = new List<Vector3>();

    /// <summary>
    /// function to genereate add only playable positions
    /// </summary>
    void InitialiseList()
    {
        gridPositions.Clear();
        for(int x = outerWall + 1; x < columns - (outerWall); x++)
        {
            for(int y = outerWall+2; y < rows - (outerWall); y++)
            {
                gridPositions.Add(new Vector3(x, y, 0f));
            }
        }
    }

    /// <summary>
    /// function to genereate map
    /// </summary>
    void BoardSetup()
    {
        columns = columnsBoard+(outerWall*2);
        rows = rowsBoard + (outerWall * 2);
        boardHolder = new GameObject().transform;
        for(int x =-1; x < columns + 1; x++)
        {
            for (int y = -1; y < rows + 1; y++)
            {
                //instanciate floor tiles
                GameObject toInstantiate = floorTiles[Random.Range(0,6)];
                if(x <= outerWall || x >=columns - outerWall || y <= outerWall || y >= rows - outerWall)
                {
                    //instanciate outerwall tiles 
                    toInstantiate = outerWallTiles[Random.Range(0, outerWallTiles.Length)];
                }
                
                GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f),Quaternion.identity)
                    as GameObject;
                instance.transform.SetParent(boardHolder);
            }
        }
    }

    /// <summary>
    /// function to get random position playable
    /// </summary>
    /// <returns>randomPosition</returns>
    Vector3 RandomPosition()
    {
        int randomIndex = Random.Range(0, gridPositions.Count);
        Vector3 randomPosition = gridPositions[randomIndex];
        gridPositions.RemoveAt(randomIndex);
        return randomPosition;
    }

    /// <summary>
    /// function to instantiate tile random in the map
    /// </summary>
    /// <param name="maximum"></param>
    /// <param name="minimum"></param>
    /// <param name="tileArray"></param>
    void LayoutObjectAtRandom(GameObject[] tileArray,int minimum,int maximum)
    {
        int objectCount = Random.Range(minimum, maximum + 1);
        for(int i = 0; i < objectCount; i++)
        {
            Vector3 randomPosition = RandomPosition();
            GameObject tileChoice = tileArray[Random.Range(0, tileArray.Length)];
            Instantiate(tileChoice, randomPosition, Quaternion.identity);
        }
    }

    /// <summary>
    /// function to instantiate tile particular in the map
    /// </summary>
    /// <param name="tileArray"></param>
    void LayoutObjectAtRandom(GameObject tileArray)
    {
            Vector3 randomPosition = RandomPosition();
            GameObject tileChoice = tileArray;
            Instantiate(tileChoice, randomPosition, Quaternion.identity);
    }

    /// <summary>
    /// function to instantiate tile particulara few times in the map
    /// </summary>
    /// <param name="tileArray"></param>
    /// <param name="howMany"></param>
    void LayoutObjectAtRandom(GameObject tileArray,int howMany)
    {
        for (int i = 0; i < howMany; i++)
        {
            Vector3 randomPosition = RandomPosition();
            GameObject tileChoice = tileArray;
            Instantiate(tileChoice, randomPosition, Quaternion.identity);
        }
    }

    /// <summary>
    /// function to instantiate all elements needed
    /// </summary>
    /// <param name="level"></param>
    public void SetupScene(int level)
    {
        //generate minimum and maximum
        wallCount = new Count(wallCountMin, wallCountMax);
        foodCount = new Count(foodCountMin, foodCountMax);
        enemiesCount = new Count(enemiesCountMin,enemiesCountMax);
        goldCount = new Count(goldCountMin, goldCountMax);
        harmfulObjetcsCount = new Count(harmfulObjetcsCountMin, harmfulObjetcsCountMax);
        //load map
        BoardSetup();
        InitialiseList();
        //load objects in map
        LayoutObjectAtRandom(wallTiles, wallCount.minimum, wallCount.maximum);
        LayoutObjectAtRandom(onlyPotionsTiles, foodCount.minimum, foodCount.maximum);
        LayoutObjectAtRandom(enemyTiles, enemiesCount.minimum, enemiesCount.maximum);
        LayoutObjectAtRandom(harmfulObjects, harmfulObjetcsCount.minimum, harmfulObjetcsCount.maximum);
        LayoutObjectAtRandom(exit);
        int howMany = Random.Range(goldCount.minimum, goldCount.maximum);
        LayoutObjectAtRandom(gold, howMany);
        Instantiate(player, new Vector3((outerWall + 1), (outerWall + 1), 0f), Quaternion.identity);
    }

}

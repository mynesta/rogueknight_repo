﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Login : MonoBehaviour
{
    //variables
    private int scene;
    public InputField password;
    public InputField username;
    private string message;
    public Text messageLog;

    private string url = "mysqlantimateria.ddns.net";
    private int port = 12345;
    private bool correctUser = false;
    public static string user;
    public static string pass;
    

    /// <summary>
    /// function to exit game
    /// </summary>
    public void exitGame()
    {
        Application.Quit();
    }

    /// <summary>
    /// function to do login
    /// </summary>
    public void login()
    {
        //fill fields
        if(username.text != "" && password.text != "")
        {
            //get user
            user = username.text;
            pass = password.text;
            //try login
            Thread login = new Thread(new ThreadStart(threadOnlyLogin));
            login.Start();
            login.Join();
            if (correctUser)
            {
                //load data, detect if user have any player alive
                GameManager gameManager = new GameManager();
                gameManager.initialThreads();
                if (gameManager.playerLifePoints < 1)
                {
                    scene = SceneManager.GetActiveScene().buildIndex + 3;
                    SceneManager.LoadScene(scene);
                }
                else
                {
                    scene = SceneManager.GetActiveScene().buildIndex + 2;
                    SceneManager.LoadScene(scene);
                }
            }
        }
        else
        {
            message = "Missing fields to fill";
        }
        messageLog.text = message;
    }

    /// <summary>
    /// function to do change scente to the tutorial screen
    /// </summary>
    public void tutorial()
    {
        scene = SceneManager.GetActiveScene().buildIndex + 7;
        SceneManager.LoadScene(scene);
    }

    /// <summary>
    /// function to change scene to the register screen
    /// </summary>
    public void register()
    {
        scene = SceneManager.GetActiveScene().buildIndex + 1;
        SceneManager.LoadScene(scene);
    }

    /// <summary>
    /// thread to do login
    /// </summary>
    private void threadOnlyLogin()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(username.text, clientSocket);
        sendString(password.text, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("login", clientSocket);
            message = "";
            correctUser = true;
        }
        else
        {
            sendString("error", clientSocket);
            message = "Wrong user";
            correctUser = false;
        }
    }

    /// <summary>
    ///function for receive data from server
    /// </summary>
    /// <param name="clientSocket"></param>
    /// <returns>rcv</returns>
    private String receiveString(Socket clientSocket)
    {
        byte[] rcvLenBytes = new byte[4];
        clientSocket.Receive(rcvLenBytes);
        int rcvLen = System.BitConverter.ToInt32(rcvLenBytes, 0);
        byte[] rcvBytes = new byte[rcvLen];
        clientSocket.Receive(rcvBytes);
        String rcv = System.Text.Encoding.ASCII.GetString(rcvBytes);
        return rcv;
    }

    /// <summary>
    ///function for send data to server
    /// </summary>
    /// <param name="toSend"></param>
    /// <param name="clientSocket"></param>
    private void sendString(String toSend, Socket clientSocket)
    {
        int toSendLen = System.Text.Encoding.ASCII.GetByteCount(toSend);
        byte[] toSendBytes = System.Text.Encoding.ASCII.GetBytes(toSend);
        byte[] toSendLenBytes = System.BitConverter.GetBytes(toSendLen);
        clientSocket.Send(toSendLenBytes);
        clientSocket.Send(toSendBytes);
    }
}

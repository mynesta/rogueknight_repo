﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MovingObject
{
    //variables
    private int playerDamage;
    public GameObject gold;
    private Animator animator;
    private Transform target;
    private bool skipMove;
    private int wallDamage = 2;
    public AudioClip enemyAttack1;
    public AudioClip enemyAttack2;
    [HideInInspector]public bool alive;
    public bool canDestroyWall;
    private int levelEnemy;
    public int hitPoints;
    private int attack;
    public int defense;
    public bool type;
    private int hitPointsBase;
    private int attackBase;
    private int defenseBase;
    public AudioClip chopSound1;
    public AudioClip chopSound2;
    public bool attackWallEnabled;

    /// <summary>
    /// start enemy
    /// </summary>
    protected override void Start()
    {
        alive = true;
        GameManager.instance.AddEnemiesToList(this);
        animator = GetComponent<Animator>();
        target = GameObject.Find("Player(Clone)").transform;
        //level enemy
        levelEnemy = ((GameManager.instance.gameCheckpoint)/6)+1;
        //depends on the enemy has some statistics or other
        if (!type)
        {
            hitPointsBase = 5;
            attackBase = 3;
            defenseBase = 2;
        }
        else
        {
            hitPointsBase = 10;
            attackBase = 5;
            defenseBase = 4;
        }
        //progress level enemy
        if(levelEnemy > 1)
        {
            hitPoints = hitPointsBase * levelEnemy;
            attack = (levelEnemy * attackBase) + attackBase;
            defense = defenseBase + (levelEnemy * defenseBase);
            
        }
        else
        {
            hitPoints = hitPointsBase;
            attack = attackBase;
            defense = defenseBase;
        }
        base.Start();
    }

    /// <summary>
    /// function check that enemies can move
    /// <param name="xDir"/>
    /// <param name="yDir"/>
    /// </summary>
    protected override void AttemptMove<T>(int xDir,int yDir)
    {
        if (skipMove)
        {
            skipMove = false;
            return;
        }
        //checks where enemies move
        base.AttemptMove<T>(xDir, yDir);
        skipMove = true;
        
    }

    /// <summary>
    /// move enemies
    /// </summary>
    public void MoveEnemy()
    {
        int xDir = 0;
        int yDir = 0;
        
        if(Mathf.Abs(target.position.x - transform.position.x) < float.Epsilon)
        {
            yDir = target.position.y > transform.position.y ? 1 : -1;
        }
        else
        {
            xDir = target.position.x > transform.position.x ? 1 : -1;
           
        }
        //move towards the player
        AttemptMove<Player>(xDir, yDir);
    }

    /// <summary>
    /// function for detect colisions with the enemies
    /// </summary>
    /// <param name="collider2D"></param>
    void OnTriggerEnter2D(Collider2D collider2D) 
    {
        //destroy potions of the map
        if (collider2D.tag == "Potion")
        {
            collider2D.gameObject.SetActive(false);
            
        }
        //receive damage with harmfulobjects
        else if (collider2D.tag == "harmfulObject")
        {
            hitPoints -= 2;
            if (hitPoints < 1)
            {
                gameObject.SetActive(false);
                alive = false;
            }
            animator.SetTrigger("enemyHit");
        }
    }

    /// <summary>
    /// function for enemies to attack the player
    /// </summary>
    /// <param name="component"></param>
    protected override void OnCantMove<T>(T component)
    {
        Player hitPlayer = component as Player;
        animator.SetTrigger("enemyAttack");
        playerDamage = combatSystem(attack, Player.defense);
        hitPlayer.LoseLife(playerDamage);
        SoundManager.instance.RandomizeSfx(enemyAttack1, enemyAttack2);
    }

    /// <summary>
    /// function for enemies to attack the walls
    /// </summary>
    /// <param name="component"></param>
    protected override void EnemyAttackWall<T>(T component){
        if (attackWallEnabled)
        {
            Wall hitWall = component as Wall;
            animator.SetTrigger("enemyAttack");
            hitWall.DamageWall(wallDamage);
            SoundManager.instance.RandomizeSfx(enemyAttack1);
        }
    }

    protected override void PlayerAttackEnemy<T>(T component){}

    /// <summary>
    /// function for calculate damage (combat system)
    /// </summary>
    public int combatSystem(int attack,int defense)
    {
        int damage = attack - defense;
        if (damage < 1)
        {
            damage = 1;
        }
        return damage;
    }

    /// <summary>
    /// function to subtract life with the damage received
    /// </summary>
    /// <param name="loss"></param>
    public void LoseLife(int loss)
    {
        SoundManager.instance.RandomizeSfx(chopSound1, chopSound2);
        hitPoints -= loss;
        animator.SetTrigger("enemyHit");
        if (hitPoints < 1)
        {
            int howMany = Random.Range(1, 11);
            LayoutObjectAtEnemyPosition(gold, howMany*levelEnemy);
            Destroy(gameObject);
            alive = false;
        }
    }

    /// <summary>
    /// function to generate gold when enemy dies
    /// </summary>
    /// <param name="howMany"></param>
    /// <param name="tileArray"></param>
    void LayoutObjectAtEnemyPosition(GameObject tileArray, int howMany)
    {
        GameObject tileChoice = tileArray;
        Instantiate(tileChoice, transform.position, Quaternion.identity);
    }

}

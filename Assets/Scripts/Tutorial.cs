﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Tutorial : MonoBehaviour
{
    private int scene;

    /// <summary>
    ///function to back login screen
    /// </summary>
    public void backLogin()
    {
        scene = SceneManager.GetActiveScene().buildIndex - 7;
        SceneManager.LoadScene(scene);
    }
}

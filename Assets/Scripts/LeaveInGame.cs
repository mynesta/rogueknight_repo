﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LeaveInGame : MonoBehaviour
{
    //variables
    private int scene;
    private GameObject gameManager;
    private GameObject screenLeave;
    Text goldTotal;
    Text enemiesTotal;
    Text itemsTotal;
    Text generalMessage;
    Text bloodPointsTotal;
    Text messageIntro;
    public float levelStartDelay = 2f;
    public int gameCheckpoint;
    public int gameGoldObtained;
    public int gameItemsObtained;
    public int gameExperienceObtained;
    public int gameEnemiesEliminated;

    private string user;
    private string password;
    private int gameID;
    private bool soundOn;
    private Button moveUp;
    private Button moveDown;
    private Button moveLeft;
    private Button moveRight;
    public Image showMute;
    public Sprite imageMute;
    public Sprite imageUnmute;

    /// <summary>
    /// start leaveInGame (script for button leave, button mute and buttons controls mobile)
    /// </summary>
    private void Start()
    {
        //control sound
        soundOn = true;
        //get user
        user = Login.user;
        password = Login.pass;
        //get components
        gameManager = GameObject.Find("GameManager(Clone)");
        screenLeave = GameObject.Find("LevelImage");
        goldTotal = GameObject.Find("TotalGoldGame").GetComponent<Text>();
        generalMessage = GameObject.Find("LevelText").GetComponent<Text>();
        enemiesTotal = GameObject.Find("TotalEnemiesEliminated").GetComponent<Text>();
        itemsTotal = GameObject.Find("TotalItemsObtained").GetComponent<Text>();
        bloodPointsTotal = GameObject.Find("TotalBloodPoints").GetComponent<Text>();
        messageIntro = GameObject.Find("MessageIntro").GetComponent<Text>();
        moveUp = GameObject.Find("MoveUp").GetComponent<Button>();
        moveDown = GameObject.Find("MoveDown").GetComponent<Button>();
        moveLeft = GameObject.Find("MoveLeft").GetComponent<Button>();
        moveRight = GameObject.Find("MoveRight").GetComponent<Button>();
        showMute = GameObject.Find("ImageMute").GetComponent<Image>();
    }

    /// <summary>
    /// function to leave game, return the main menu
    /// </summary>
    public void leaveGame()
    {
        //calculate and update bloodpoints from user
        GameManager.instance.calculateBloodPoints();
        GameManager.instance.updateBloodPoints();
        //update game
        int lifePlayer = Player.life;
        Player.life = 0;
        GameManager.instance.updateGame();
        Player.life = lifePlayer;
        //others updates
        GameManager.instance.updateCharacter();
        GameManager.instance.updateInventory();
        //update texts
        generalMessage.text = "You have left the dungeon, reaching as far as the floor " + GameManager.instance.level;
        goldTotal.text =  "Gold: " + GameManager.instance.gameGoldObtained;
        enemiesTotal.text =  "Enemies Eliminated: " + GameManager.instance.gameEnemiesEliminated;
        itemsTotal.text =  "Items Obtained: " + GameManager.instance.gameItemsObtained;
        bloodPointsTotal.text = "Bloodpoints: " + GameManager.instance.bloodPointsNew;
        messageIntro.text = "";
        //activated black screen
        screenLeave.GetComponent<Image>().enabled = true;
        SoundManager.instance.efxSource.Pause();
        SoundManager.instance.musicSource.Pause();
        Destroy(gameManager);
        Invoke("HideLevelImage", levelStartDelay);
        Invoke("loadScene", 1);
        

    }

    /// <summary>
    /// function to mute or unmute game
    /// </summary>
    public void mute()
    {
        if (SoundManager.instance.canActivate)
        {
            SoundManager.instance.musicSource.Pause();
            SoundManager.instance.efxSource.Pause();
            soundOn = false;
            showMute.sprite = imageMute;
            SoundManager.instance.canActivate = false;
        }
        else
        {
            SoundManager.instance.musicSource.Play();
            SoundManager.instance.efxSource.Play();
            SoundManager.instance.canActivate = true;
            showMute.sprite = imageUnmute;
            soundOn = true;
        }
        
    }

    /// <summary>
    /// function to controls of mobile
    /// </summary>
    public void buttonClicked()
    {
        moveUp.GetComponent<ButtonsMove>().activated = false;
        moveDown.GetComponent<ButtonsMove>().activated = false;
        moveLeft.GetComponent<ButtonsMove>().activated = false;
        moveRight.GetComponent<ButtonsMove>().activated = false;
        //detect pressed button
        EventSystem.current.currentSelectedGameObject.GetComponent<ButtonsMove>().activated = true;
    }

    /// <summary>
    /// function to change scene to the main menu
    /// </summary>
    private void loadScene()
    {
        scene = SceneManager.GetActiveScene().buildIndex - 2;
        SceneManager.LoadScene(scene);
    }

    /// <summary>
    /// function to erase text and deactivate the black screen
    /// </summary>
    private void HideLevelImage()
    {
        
        screenLeave.GetComponent<Image>().enabled = false;
        generalMessage.text = "";
        goldTotal.text = "";
        enemiesTotal.text = "";
        itemsTotal.text = "";
        messageIntro.text = "";
    }
}

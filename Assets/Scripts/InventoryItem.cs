﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItem
{
    //variables
    public String item;
    public int idItem;
    public Sprite sprite;
    public int quantity;
    public bool equip;
    public int sellCost;
    public int buyCost;

    /// <summary>
    ///constructor class (create object)
    /// </summary>
    /// <param name="buyCost"></param>
    /// <param name="equip"></param>
    /// <param name="idItem"></param>
    /// <param name="item"></param>
    /// <param name="quantity"></param>
    /// <param name="sellCost"></param>
    /// <param name="sprite"></param>
    public InventoryItem(String item, int idItem, Sprite sprite, int quantity, bool equip, int sellCost,int buyCost)
    {
        this.item = item;
        this.idItem = idItem;
        this.sprite = sprite;
        this.quantity = quantity;
        this.equip = equip;
        this.sellCost = sellCost;
        this.buyCost = buyCost;
    }
}

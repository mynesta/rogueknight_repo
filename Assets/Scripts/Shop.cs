﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Events;

public class Shop : MonoBehaviour
{
    //variables
    private int scene;
    private Text level;
    public List<InventoryItem> items = new List<InventoryItem>();
    public GameObject item;
    private Transform inventoryUI;

    public int playerLifePoints;
    public int maxPlayerLifePoints;
    public int playerAttack;
    public int playerDefense;
    public String nickname;
    public int playerLevel;
    public int inventorySlots;
    public int experience;
    public int needExperience;
    public int gold;
    private int characterID;
    private int itemsCount;
    public string user;
    public string password;

    private int itemsCountForLimit;
    private int iterationItem;
    private int iterationItemLoad;
    private int idItem;
    private string nameItem;
    private int quantityItem;
    private bool equipItem;

    public GameObject[] itemsTiles;

    private Text maxItemsInventoryText;
    private Text goldText;
    private Text messageText;
    private string message;

    public int gameCheckpoint;
    public int gameGoldObtained;
    public int gameItemsObtained;
    public int gameExperienceObtained;
    public int gameEnemiesEliminated;
    private int gameID;

    private string url = "mysqlantimateria.ddns.net";
    private int port = 12345;

    /// <summary>
    /// start shop
    /// </summary>
    void Start()
    {
        //get user
        user = Login.user;
        password = Login.pass;
        //get components
        level = GameObject.Find("LevelShopText").GetComponent<Text>();
        Debug.Log("tienda inventory: " + items.Count);
        inventoryUI = GameObject.Find("InventoryLayoutShop").GetComponent<Transform>();
        maxItemsInventoryText = GameObject.Find("MaxItemInventory").GetComponent<Text>();
        goldText = GameObject.Find("Gold").GetComponent<Text>();
        messageText = GameObject.Find("Message").GetComponent<Text>();
        //load data
        Thread loadDataPlayer = new Thread(new ThreadStart(threadLoadDataPlayer));
        loadDataPlayer.Start();
        loadDataPlayer.Join();
        Thread loadDataGame = new Thread(new ThreadStart(threadLoadDataGame));
        loadDataGame.Start();
        loadDataGame.Join();
        loadItems();
        loadItemsUI();
        //update texts
        goldText.text = "Gold: " + gold;
        maxItemsInventoryText.text = inventorySlots + " slots";
        level.text = "Level: " + GameManager.instance.level;

    }

    /// <summary>
    /// threat to update game
    /// </summary>
    private void threadUpdateGame()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("update", clientSocket);
            sendString("game", clientSocket);
            //update game
            sendString(Convert.ToString(gameID), clientSocket);
            sendString(Convert.ToString(false), clientSocket);
            sendString(Convert.ToString(gameCheckpoint), clientSocket);
            sendString(Convert.ToString(gameGoldObtained), clientSocket);
            sendString(Convert.ToString(gameItemsObtained), clientSocket);
            sendString(Convert.ToString(gameCheckpoint), clientSocket);
            sendString(Convert.ToString(gameExperienceObtained), clientSocket);
            sendString(Convert.ToString(gameEnemiesEliminated), clientSocket);
        }
    }

    /// <summary>
    /// function to call update game
    /// </summary>
    private void updateGame()
    {
        Thread updateGame = new Thread(new ThreadStart(threadUpdateGame));
        updateGame.Start();
        updateGame.Join();
    }

    /// <summary>
    /// function to call load player
    /// </summary>
    private void loadPlayer()
    {
        Thread loadDataPlayer = new Thread(new ThreadStart(threadLoadDataPlayer));
        loadDataPlayer.Start();
        loadDataPlayer.Join();
    }

    /// <summary>
    /// thread to load game
    /// </summary>
    private void threadLoadDataGame()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("select", clientSocket);
            sendString("game", clientSocket);
            //load game
            gameID = Convert.ToInt32(receiveString(clientSocket));
            gameCheckpoint = Convert.ToInt32(receiveString(clientSocket));
            gameGoldObtained = Convert.ToInt32(receiveString(clientSocket));
            gameItemsObtained = Convert.ToInt32(receiveString(clientSocket));
            gameExperienceObtained = Convert.ToInt32(receiveString(clientSocket));
            gameEnemiesEliminated = Convert.ToInt32(receiveString(clientSocket));
        }
    }

    /// <summary>
    /// function to continue game
    /// </summary>
    public void continueGame()
    {
        //change scene the game and load items
        scene = SceneManager.GetActiveScene().buildIndex - 1;
        SceneManager.LoadScene(scene);
        GameManager.instance.resetItems();

    }

    /// <summary>
    /// function to delete items UI
    /// </summary>
    public void deleteAllItemsUI()
    {

        for (int i = 0; i < inventoryUI.childCount; i++)
        {
            Destroy(inventoryUI.GetChild(i).gameObject);
        }
    }

    /// <summary>
    /// function to load items UI
    /// </summary>
    public void loadItemsUI()
    {
        itemsCountForLimit = 0;
        for (int i = 0; i < items.Count; i++)
        {
            for (int l = 0; l < (items[i].quantity); l++)
            {
                if (items[i].quantity > 0)
                {
                    itemsCountForLimit += 1;
                    //load image
                    item.GetComponent<Image>().sprite = items[i].sprite;
                    //load item and sale price (text)
                    item.transform.GetChild(0).GetComponent<Text>().text = items[i].item;
                    item.transform.GetChild(1).GetComponent<Text>().text = items[i].sellCost + " Gold";
                    GameObject instantiated = Instantiate(item, inventoryUI);
                    //add buttons and function to the items
                    switch (items[i].idItem)
                    {
                        case 1:
                            instantiated.GetComponent<Button>().onClick.AddListener(removeItemLeatherArmor);
                            break;
                        case 2:
                            instantiated.GetComponent<Button>().onClick.AddListener(removeItemPlateMail);
                            break;
                        case 3:
                            instantiated.GetComponent<Button>().onClick.AddListener(removeItemPotion);
                            break;
                        case 4:
                            instantiated.GetComponent<Button>().onClick.AddListener(removeItemQualityPotion);
                            break;
                        case 5:
                            instantiated.GetComponent<Button>().onClick.AddListener(removeItemHeroicPotion);
                            break;
                        case 6:
                            instantiated.GetComponent<Button>().onClick.AddListener(removeItemDivinePotion);
                            break;
                        case 7:
                            instantiated.GetComponent<Button>().onClick.AddListener(removeItemBroadsword);
                            break;
                        case 8:
                            instantiated.GetComponent<Button>().onClick.AddListener(removeItemWarAxe);
                            break;
                    }
                    
                }
            }
        }

    }

    /// <summary>
    /// thread to the load player
    /// </summary>
    private void threadLoadDataPlayer()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("select", clientSocket);
            sendString("player", clientSocket);
            //load data player
            sendString(user, clientSocket);
            if (Convert.ToBoolean(receiveString(clientSocket)))
            {
                characterID = Convert.ToInt32(receiveString(clientSocket));
                nickname = receiveString(clientSocket);
                playerLevel = Convert.ToInt32(receiveString(clientSocket));
                int lifeDB = Convert.ToInt32(receiveString(clientSocket));
                playerLifePoints = lifeDB;
                maxPlayerLifePoints = lifeDB;
                playerAttack = Convert.ToInt32(receiveString(clientSocket));
                playerDefense = Convert.ToInt32(receiveString(clientSocket));
                inventorySlots = Convert.ToInt32(receiveString(clientSocket));
                experience = Convert.ToInt32(receiveString(clientSocket));
                needExperience = Convert.ToInt32(receiveString(clientSocket));
                gold = Convert.ToInt32(receiveString(clientSocket));
                itemsCount = Convert.ToInt32(receiveString(clientSocket));
            }


        }
    }

    /// <summary>
    /// thread to update character
    /// </summary>
    private void threadUpdateCharacter()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("update", clientSocket);
            sendString("character", clientSocket);
            //update character
            sendString(Convert.ToString(characterID), clientSocket);
            sendString(Convert.ToString(true), clientSocket);
            sendString(Convert.ToString(Player.levelPlayer), clientSocket);
            sendString(Convert.ToString(Player.maxLife), clientSocket);
            sendString(Convert.ToString(Player.attack), clientSocket);
            sendString(Convert.ToString(Player.defense), clientSocket);
            sendString(Convert.ToString(Player.inventorySlotsMax), clientSocket);
            sendString(Convert.ToString(Player.experience), clientSocket);
            sendString(Convert.ToString(Player.maxExperience), clientSocket);
            sendString(Convert.ToString(gold), clientSocket);
        }
    }

    /// <summary>
    /// function to call update player (character)
    /// </summary>
    private void updatePlayer()
    {
        Thread updateCharacter = new Thread(new ThreadStart(threadUpdateCharacter));
        updateCharacter.Start();
        updateCharacter.Join();
    }

    /// <summary>
    /// function to call update items
    /// </summary>
    private void updateItems()
    {
        for (iterationItem = 0; iterationItem < items.Count; iterationItem++)
        {
            Thread updateInventory = new Thread(new ThreadStart(threadUpdateItemInventory));
            updateInventory.Start();
            updateInventory.Join();
        }
    }

    /// <summary>
    /// thread to update items inventory
    /// </summary>
    private void threadUpdateItemInventory()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("update", clientSocket);
            sendString("itemInventory", clientSocket);
            //update item
            sendString(Convert.ToString(characterID), clientSocket);
            sendString(Convert.ToString(Convert.ToString(items[iterationItem].idItem)), clientSocket);
            sendString(Convert.ToString(Convert.ToString(items[iterationItem].quantity)), clientSocket);
        }
    }

    /// <summary>
    ///function for receive data from server
    /// </summary>
    /// <param name="clientSocket"></param>
    /// <returns>rcv</returns>
    private String receiveString(Socket clientSocket)
    {
        byte[] rcvLenBytes = new byte[4];
        clientSocket.Receive(rcvLenBytes);
        int rcvLen = System.BitConverter.ToInt32(rcvLenBytes, 0);
        byte[] rcvBytes = new byte[rcvLen];
        clientSocket.Receive(rcvBytes);
        String rcv = System.Text.Encoding.ASCII.GetString(rcvBytes);
        return rcv;
    }

    /// <summary>
    ///function for send data to server
    /// </summary>
    /// <param name="toSend"></param>
    /// <param name="clientSocket"></param>
    private void sendString(String toSend, Socket clientSocket)
    {
        int toSendLen = System.Text.Encoding.ASCII.GetByteCount(toSend);
        byte[] toSendBytes = System.Text.Encoding.ASCII.GetBytes(toSend);
        byte[] toSendLenBytes = System.BitConverter.GetBytes(toSendLen);
        clientSocket.Send(toSendLenBytes);
        clientSocket.Send(toSendBytes);
    }

    /// <summary>
    /// functio to load items in a list
    /// </summary>
    public void loadItems()
    {
        items.Clear();
        for (iterationItemLoad = 0; iterationItemLoad < itemsCount; iterationItemLoad++)
        {
            Thread loadDataItems = new Thread(new ThreadStart(threadLoadItemInventory));
            loadDataItems.Start();
            loadDataItems.Join();
            //create object
            InventoryItem inventoryItem = new InventoryItem(nameItem, idItem, itemsTiles[idItem - 1].GetComponent<SpriteRenderer>().sprite,
                quantityItem, equipItem, itemsTiles[idItem - 1].GetComponent<Item>().sellCost, itemsTiles[idItem - 1].GetComponent<Item>().buyCost);
            items.Add(inventoryItem);
        }
    }

    /// <summary>
    /// thread to load items inventory
    /// </summary>
    private void threadLoadItemInventory()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("select", clientSocket);
            sendString("itemInventory", clientSocket);
            //load items inventory
            sendString(Convert.ToString(characterID), clientSocket);
            sendString(Convert.ToString(iterationItemLoad), clientSocket);
            idItem = Convert.ToInt32(receiveString(clientSocket));
            nameItem = receiveString(clientSocket);
            quantityItem = Convert.ToInt32(receiveString(clientSocket));
            equipItem = Convert.ToBoolean(receiveString(clientSocket));
        }
    }

    /// <summary>
    /// function to buy any item
    /// </summary>
    /// <param name="idItem"></param>
    private void buyItemGeneral(int idItem)
    {
        if (itemsCount < inventorySlots)
        {
            //call function to add item
            switch (idItem)
            {
                case 1:
                    addItem("Leather Armor", 1, itemsTiles[0].GetComponent<SpriteRenderer>().sprite, 1, true, itemsTiles[0].GetComponent<Item>().sellCost, itemsTiles[0].GetComponent<Item>().buyCost);
                    break;
                case 2:
                    addItem("Plate Mail", 2, itemsTiles[1].GetComponent<SpriteRenderer>().sprite, 1, true, itemsTiles[1].GetComponent<Item>().sellCost, itemsTiles[1].GetComponent<Item>().buyCost);
                    break;
                case 3:
                    addItem("Potion", 3, itemsTiles[2].GetComponent<SpriteRenderer>().sprite, 1, false, itemsTiles[2].GetComponent<Item>().sellCost, itemsTiles[2].GetComponent<Item>().buyCost);
                    break;
                case 4:
                    addItem("Quality Potion", 4, itemsTiles[3].GetComponent<SpriteRenderer>().sprite, 1, false, itemsTiles[3].GetComponent<Item>().sellCost, itemsTiles[3].GetComponent<Item>().buyCost);
                    break;
                case 5:
                    addItem("Heroic Potion", 5, itemsTiles[4].GetComponent<SpriteRenderer>().sprite, 1, false, itemsTiles[4].GetComponent<Item>().sellCost, itemsTiles[4].GetComponent<Item>().buyCost);
                    break;
                case 6:
                    addItem("Divine Potion", 6, itemsTiles[5].GetComponent<SpriteRenderer>().sprite, 1, false, itemsTiles[5].GetComponent<Item>().sellCost, itemsTiles[5].GetComponent<Item>().buyCost);
                    break;
                case 7:
                    addItem("Broadsword", 7, itemsTiles[6].GetComponent<SpriteRenderer>().sprite, 1, false, itemsTiles[6].GetComponent<Item>().sellCost, itemsTiles[6].GetComponent<Item>().buyCost);
                    break;
                case 8:
                     addItem("War Axe", 8, itemsTiles[7].GetComponent<SpriteRenderer>().sprite, 1, false, itemsTiles[7].GetComponent<Item>().sellCost, itemsTiles[7].GetComponent<Item>().buyCost);
                    break;
            }
        }
    }

    /// <summary>
    /// function to call buy item
    /// </summary>
    public void buyItemLeatherArmor()
    {
        buyItemGeneral(1);
    }

    /// <summary>
    /// function to call buy item
    /// </summary>
    public void buyItemPlateMail()
    {
        buyItemGeneral(2);
    }

    /// <summary>
    /// function to call buy item
    /// </summary>
    public void buyItemPotion()
    {
        buyItemGeneral(3);
    }

    /// <summary>
    /// function to call buy item
    /// </summary>
    public void buyItemQualityPotion()
    {
        buyItemGeneral(4);
    }

    /// <summary>
    /// function to call buy item
    /// </summary>
    public void buyItemHeroicPotion()
    {
        buyItemGeneral(5);
    }

    /// <summary>
    /// function to call buy item
    /// </summary>
    public void buyItemDivinePotion()
    {
        buyItemGeneral(6);
    }

    /// <summary>
    /// function to call buy item
    /// </summary>
    public void buyItemBroadsword()
    {
        buyItemGeneral(7);
    }

    /// <summary>
    /// function to call buy item
    /// </summary>
    public void buyItemWarAxe()
    {
        buyItemGeneral(8);
    }

    /// <summary>
    /// coroutine to show message for a few seconds
    /// </summary>
    /// <param name="delay"></param>
    /// <returns></returns>
    IEnumerator ShowMessage(float delay)
    {
        yield return new WaitForSeconds(delay);
        messageText.text = "";
    }

    /// <summary>
    /// functio to add item
    /// </summary>
    /// <param name="name"></param>
    /// <param name="id"></param>
    /// <param name="sprite"></param>
    /// <param name="quantity"></param>
    /// <param name="equip"></param>
    /// <param name="sellCost"></param>
    /// <param name="buyCost"></param>
    private void addItem(string name, int id, Sprite sprite, int quantity, bool equip, int sellCost,int buyCost)
    {
        if (itemsCountForLimit < inventorySlots)
        {
            //need minimum of gold
            if (gold >= buyCost)
            {
                bool exist = false;
                for (int i = 0; i < items.Count; i++)
                {
                    if (id == items[i].idItem)
                    {
                        exist = true;
                        //can only have 1 equip of each 1
                        if (!items[i].equip)
                        {
                            items[i].quantity += 1;
                            gold -= buyCost;
                            gameItemsObtained += 1;
                            message = name + " bought";
                            break;
                        }
                    }
                }
                //create object if doesn't exit
                if (!exist)
                {
                    InventoryItem inventoryItem = new InventoryItem(name, id, sprite, quantity, equip, sellCost,buyCost);
                    items.Add(inventoryItem);
                    gold -= buyCost;
                    message = name + " bought";
                }
                //update data
                updatePlayer();
                updateItems();
                loadPlayer();
                loadItems();
                deleteAllItemsUI();
                loadItemsUI();
                updateGame();
                goldText.text = gold + " Gold";
            }
            else
            {
                message = "lack of gold";
            }
        }
        else
        {
            Debug.Log("limite");
            message = "limit reached";
        }
        messageText.text = message;
        StartCoroutine(ShowMessage(2));
    }

    /// <summary>
    /// function to sell objects
    /// </summary>
    /// <param name="id"></param>
    /// <param name="sellCost"></param>
    private void removeItemGeneral(int id,int sellCost)
    {
        for (int i = 0; i < items.Count; i++)
        {
            //sell
            if (id == items[i].idItem)
            {
                if (items[i].quantity > 0)
                {
                    items[i].quantity -= 1;
                    gold += sellCost;
                    //receive gold
                    gameGoldObtained += sellCost;
                    message = name + " sold";
                    break;
                }
            }
        }
        //update data
        updatePlayer();
        updateItems();
        loadPlayer();
        loadItems();
        deleteAllItemsUI();
        loadItemsUI();
        updateGame();
        goldText.text = gold + " Gold";

    }

    /// <summary>
    /// functio to call sell item
    /// </summary>
    public void removeItemLeatherArmor()
    {
        removeItemGeneral(1, itemsTiles[0].GetComponent<Item>().sellCost);
    }

    /// <summary>
    /// functio to call sell item
    /// </summary>
    public void removeItemPlateMail()
    {
        removeItemGeneral(2, itemsTiles[1].GetComponent<Item>().sellCost);
    }

    /// <summary>
    /// functio to call sell item
    /// </summary>
    public void removeItemPotion()
    {
        removeItemGeneral(3, itemsTiles[2].GetComponent<Item>().sellCost);
    }

    /// <summary>
    /// functio to call sell item
    /// </summary>
    public void removeItemQualityPotion()
    {
        removeItemGeneral(4, itemsTiles[3].GetComponent<Item>().sellCost);
    }

    /// <summary>
    /// functio to call sell item
    /// </summary>
    public void removeItemHeroicPotion()
    {
        removeItemGeneral(5, itemsTiles[4].GetComponent<Item>().sellCost);
    }

    /// <summary>
    /// functio to call sell item
    /// </summary>
    public void removeItemDivinePotion()
    {
        removeItemGeneral(6, itemsTiles[5].GetComponent<Item>().sellCost);
    }

    /// <summary>
    /// functio to call sell item
    /// </summary>
    public void removeItemBroadsword()
    {
        removeItemGeneral(7, itemsTiles[6].GetComponent<Item>().sellCost);
    }

    /// <summary>
    /// functio to call sell item
    /// </summary>
    public void removeItemWarAxe()
    {
        removeItemGeneral(8, itemsTiles[7].GetComponent<Item>().sellCost);
    }

    /// <summary>
    /// functio to leave game from store, return main menu
    /// </summary>
    public void leaveGame()
    {
        GameManager.instance.calculateBloodPoints();
        GameManager.instance.updateBloodPoints();
        SoundManager.instance.efxSource.Pause();
        SoundManager.instance.musicSource.Pause();
        scene = SceneManager.GetActiveScene().buildIndex - 3;
        SceneManager.LoadScene(scene);
    }
}

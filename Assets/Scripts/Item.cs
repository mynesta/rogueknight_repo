﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    //variables to the inspecter
    public int id;
    public string nameItem;
    public bool equip;
    public bool healing;
    public bool increaseAttack;
    public bool increaseDefense;
    public int sellCost;
    public int buyCost;
    public bool usingEquipable;

}

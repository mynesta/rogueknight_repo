﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using ThreadPriority = System.Threading.ThreadPriority;


public class GameManager : MonoBehaviour
{
    //variables
    public Sprite imageMute;
    public float levelStartDelay;
    public int incrementBoard;
    public float turnDelay;
    public static GameManager instance = null;
    private BoardManager boardScript;
    public int playerLifePoints;
    public int maxPlayerLifePoints;
    public int playerAttack;
    public int playerDefense;
    public int playerAttackTotal;
    public int playerDefenseTotal;
    public String nickname;
    public int playerLevel;
    public int inventorySlots;
    public int experience;
    public int needExperience;
    public int gold;
    public static bool firstFloor = false;
    private int gameID;
    private int characterID;
    private int scene;
    [HideInInspector] public bool playersTurn = true;
    public int bloodPointsCurrent;
    public int bloodPointsNew;
    private Text goldTotal;
    private Text enemiesTotal;
    private Text itemsTotal;
    private Text levelText;
    private Text bloodPointsTotal;
    private Text messageIntro;
    private GameObject levelImage;
    public int level;
    public List<Enemy> enemies;
    private bool enemiesMoving;
    private bool doingSetup =true;
    public string user;
    public string password;
    public List<InventoryItem> items = new List<InventoryItem>();

    public int gameCheckpoint;
    public int gameGoldObtained;
    public int gameItemsObtained;
    public int gameExperienceObtained;
    public int gameEnemiesEliminated;
    private int iterationItem;
    private int iterationItemLoad;
    private int idItem;
    private string nameItem;
    private int quantityItem;
    private int itemsCount;
    private bool equipItem;

    private string url = "mysqlantimateria.ddns.net";
    private int port = 12345;

    /// <summary>
    /// start game manager
    /// </summary>
    private void Start()
    {
        //get user
        user = Login.user;
        password = Login.pass;
        //load data from game manager
        playerAttackTotal=0;
        playerDefenseTotal=0;
        if (instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        enemies = new List<Enemy>();
        boardScript = GetComponent<BoardManager>();
        initialThreads();
        InitGame();
        loadItems();
    }

    /// <summary>
    /// function for load items (inventory) in a list
    /// </summary>
    public void loadItems()
    {
        for (iterationItemLoad = 0; iterationItemLoad < itemsCount; iterationItemLoad++)
        {
            Thread loadDataItems = new Thread(new ThreadStart(threadLoadItemInventory));
            loadDataItems.Start();
            loadDataItems.Join();
            //create object for all items and add in a list
            InventoryItem inventoryItem = new InventoryItem(nameItem, idItem, boardScript.itemsTiles[idItem - 1].GetComponent<SpriteRenderer>().sprite,
                quantityItem, equipItem, boardScript.itemsTiles[idItem - 1].GetComponent<Item>().sellCost, boardScript.itemsTiles[idItem - 1].GetComponent<Item>().buyCost);
            items.Add(inventoryItem);
        }
    }

    /// <summary>
    /// function for load data initial (data player, data game)
    /// </summary>
    public void initialThreads()
    {
        user = Login.user;
        password = Login.pass;
        Thread loadDataPlayer = new Thread(new ThreadStart(threadLoadDataPlayer));
        loadDataPlayer.Start();
        loadDataPlayer.Join();
        Thread loadDataGame = new Thread(new ThreadStart(threadLoadDataGame));
        loadDataGame.Start();
        loadDataGame.Join();
    }

    /// <summary>
    ///thread for load data player
    /// </summary>
    private void threadLoadDataPlayer()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("select", clientSocket);
            sendString("player", clientSocket);
            sendString(user, clientSocket);
            if (Convert.ToBoolean(receiveString(clientSocket))) 
            {
                //load data from player
                characterID = Convert.ToInt32(receiveString(clientSocket));
                nickname = receiveString(clientSocket);
                playerLevel = Convert.ToInt32(receiveString(clientSocket));
                int lifeDB = Convert.ToInt32(receiveString(clientSocket));
                playerLifePoints = lifeDB;
                maxPlayerLifePoints = lifeDB;
                playerAttack = Convert.ToInt32(receiveString(clientSocket));
                playerDefense = Convert.ToInt32(receiveString(clientSocket));
                inventorySlots = Convert.ToInt32(receiveString(clientSocket));
                experience = Convert.ToInt32(receiveString(clientSocket));
                needExperience = Convert.ToInt32(receiveString(clientSocket));
                gold = Convert.ToInt32(receiveString(clientSocket));
                itemsCount = Convert.ToInt32(receiveString(clientSocket));
            }
        }
    }

    /// <summary>
    ///thread for load items inventory
    /// </summary>
    private void threadLoadItemInventory()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("select", clientSocket);
            sendString("itemInventory", clientSocket);
            //send info to receive data from items
            sendString(Convert.ToString(characterID), clientSocket);
            sendString(Convert.ToString(iterationItemLoad), clientSocket);
            //receive data from items
            idItem = Convert.ToInt32(receiveString(clientSocket));
            nameItem = receiveString(clientSocket);
            quantityItem = Convert.ToInt32(receiveString(clientSocket));
            equipItem = Convert.ToBoolean(receiveString(clientSocket));
        }
    }

    /// <summary>
    ///thread for load data game
    /// </summary>
    private void threadLoadDataGame()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("select", clientSocket);
            sendString("game", clientSocket);
            //receive data from game
            gameID = Convert.ToInt32(receiveString(clientSocket));
            gameCheckpoint = Convert.ToInt32(receiveString(clientSocket));
            level = gameCheckpoint;
            gameGoldObtained = Convert.ToInt32(receiveString(clientSocket));
            gameItemsObtained = Convert.ToInt32(receiveString(clientSocket));
            gameExperienceObtained = Convert.ToInt32(receiveString(clientSocket));
            gameEnemiesEliminated = Convert.ToInt32(receiveString(clientSocket));
        }
    }

    /// <summary>
    ///call thread for update items in the database
    /// </summary>
    public void updateInventory()
    {
        for (iterationItem = 0; iterationItem < items.Count; iterationItem++)
        {
            Thread updateInventory = new Thread(new ThreadStart(threadUpdateItemInventory));
            updateInventory.Start();
            updateInventory.Join();
        }
        
    }

    /// <summary>
    ///thread for update item inventory
    /// </summary>
    private void threadUpdateItemInventory()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("update", clientSocket);
            sendString("itemInventory", clientSocket);
            //send info items for update
            sendString(Convert.ToString(characterID), clientSocket);
            sendString(Convert.ToString(Convert.ToString(items[iterationItem].idItem)), clientSocket);
            sendString(Convert.ToString(Convert.ToString(items[iterationItem].quantity)), clientSocket);
        }
    }

    /// <summary>
    ///thread for load blood points from user
    /// </summary>
    private void threadLoadBloodPoints()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("select", clientSocket);
            sendString("userBloodPoints", clientSocket);
            //send user name
            sendString(user, clientSocket);
            //receive blood points from user
            bloodPointsCurrent = Convert.ToInt32(receiveString(clientSocket));
        }
    }

    /// <summary>
    ///thread for update game in the database
    /// </summary>
    private void threadUpdateGame()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("update", clientSocket);
            sendString("game", clientSocket);
            // send data game for update
            sendString(Convert.ToString(gameID), clientSocket);
            Debug.Log("player life: " + Player.life);
            if (Player.life < 1)
            {
                sendString(Convert.ToString(true), clientSocket);
            }
            else
            {
                sendString(Convert.ToString(false), clientSocket);
            }
            sendString(Convert.ToString(gameCheckpoint+1), clientSocket);
            sendString(Convert.ToString(gameGoldObtained), clientSocket);
            sendString(Convert.ToString(gameItemsObtained), clientSocket);
            if(level%5 == 0)
            {
                sendString(Convert.ToString(level+1), clientSocket);
            }
            else
            {
                sendString(Convert.ToString(level), clientSocket);
            }
            sendString(Convert.ToString(gameExperienceObtained), clientSocket);
            sendString(Convert.ToString(gameEnemiesEliminated), clientSocket);
        }
    }

    /// <summary>
    ///call thread for update user's blood points
    /// </summary>
    public void updateBloodPoints()
    {
        Thread updateBloodPoints = new Thread(new ThreadStart(threadUpdateBloodPoints));
        updateBloodPoints.Start();
        updateBloodPoints.Join();
    }

    /// <summary>
    ///thread for update user's blood points in the database
    /// </summary>
    private void threadUpdateBloodPoints()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("update", clientSocket);
            sendString("user", clientSocket);
            //send user name
            sendString(user, clientSocket);
            //send the best score (old or new)
            if(bloodPointsCurrent > bloodPointsNew)
            {
                sendString(Convert.ToString(bloodPointsCurrent), clientSocket);
            }
            else
            {
                sendString(Convert.ToString(bloodPointsNew), clientSocket);
            }
            
        }
    }

    /// <summary>
    ///thread for update character data
    /// </summary>
    private void threadUpdateCharacter()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("update", clientSocket);
            sendString("character", clientSocket);
            //send character data for update
            sendString(Convert.ToString(characterID), clientSocket);
            if (Player.life < 1)
            {
                sendString(Convert.ToString(false), clientSocket);
            }
            else
            {
                sendString(Convert.ToString(true), clientSocket);
            }
            sendString(Convert.ToString(Player.levelPlayer), clientSocket);
            sendString(Convert.ToString(Player.maxLife), clientSocket);
            sendString(Convert.ToString(Player.attack), clientSocket);
            sendString(Convert.ToString(Player.defense), clientSocket);
            sendString(Convert.ToString(Player.inventorySlotsMax), clientSocket);
            sendString(Convert.ToString(Player.experience), clientSocket);
            sendString(Convert.ToString(Player.maxExperience), clientSocket);
            sendString(Convert.ToString(Player.gold), clientSocket);
        }
    }

    /// <summary>
    ///function for receive data from server
    /// </summary>
    /// <param name="clientSocket"></param>
    /// <returns>rcv</returns>
    private String receiveString(Socket clientSocket)
    {
        byte[] rcvLenBytes = new byte[4];
        clientSocket.Receive(rcvLenBytes);
        int rcvLen = System.BitConverter.ToInt32(rcvLenBytes, 0);
        byte[] rcvBytes = new byte[rcvLen];
        clientSocket.Receive(rcvBytes);
        String rcv = System.Text.Encoding.ASCII.GetString(rcvBytes);
        return rcv;
    }

    /// <summary>
    ///function for send data to server
    /// </summary>
    /// <param name="toSend"></param>
    /// <param name="clientSocket"></param>
    private void sendString(String toSend, Socket clientSocket)
    {
        int toSendLen = System.Text.Encoding.ASCII.GetByteCount(toSend);
        byte[] toSendBytes = System.Text.Encoding.ASCII.GetBytes(toSend);
        byte[] toSendLenBytes = System.BitConverter.GetBytes(toSendLen);
        clientSocket.Send(toSendLenBytes);
        clientSocket.Send(toSendBytes);
    }

    /// <summary>
    ///function for calculate new score
    /// </summary>
    public void calculateBloodPoints()
    {
        bloodPointsNew= (level * 10) + (gameEnemiesEliminated * 5) + (gameGoldObtained) + (gameItemsObtained * 2);
    }

    /// <summary>
    ///function for change floor
    /// </summary>
    /// <param name="mode"></param>
    /// <param name="scene"></param>
    static private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if(SceneManager.GetActiveScene().buildIndex == 4 && firstFloor)
        {
            instance.level++;
            instance.InitGame();
        }
    }

    /// <summary>
    ///call function for cnage floor
    /// </summary>
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    static public void CallbackInitialization()
    {
            SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    /// <summary>
    ///call thread for update character data
    /// </summary>
    public void updateCharacter()
    {
        Thread updateCharacter = new Thread(new ThreadStart(threadUpdateCharacter));
        updateCharacter.Start();
        updateCharacter.Join();
    }

    /// <summary>
    ///function called when start any floor
    /// </summary>
    void InitGame()
    {
        if (!SoundManager.instance.canActivate)
        {
            SoundManager.instance.musicSource.Pause();
            SoundManager.instance.efxSource.Pause();
            Image showMute = GameObject.Find("ImageMute").GetComponent<Image>();
            showMute.sprite = imageMute;
        }
        else
        {
            SoundManager.instance.musicSource.Play();
            SoundManager.instance.efxSource.Play();
        }
        calculateBloodPoints();
        //call thread for load blood points from user
        Thread LoadBloodPoints = new Thread(new ThreadStart(threadLoadBloodPoints));
        LoadBloodPoints.Start();
        LoadBloodPoints.Join();
        //if enter in shop ("checkpoint")
        if (instance.level %5== 0 && instance.level >1)
        {
            gameCheckpoint = level;
            updateInventory();
            updateGame();
            updateCharacter();
            //change scene to the shop
            scene = SceneManager.GetActiveScene().buildIndex + 1;
            SceneManager.LoadScene(scene);
        }
        //only load in the screen load level1
        messageIntro = GameObject.Find("MessageIntro").GetComponent<Text>();
        levelImage = GameObject.Find("LevelImage");
        levelText = GameObject.Find("LevelText").GetComponent<Text>();
        goldTotal = GameObject.Find("TotalGoldGame").GetComponent<Text>();
        enemiesTotal = GameObject.Find("TotalEnemiesEliminated").GetComponent<Text>();
        itemsTotal = GameObject.Find("TotalItemsObtained").GetComponent<Text>();
        bloodPointsTotal = GameObject.Find("TotalBloodPoints").GetComponent<Text>();
        if (instance.level == 1)
        {
            levelText.text ="" ;
            bloodPointsTotal.text = "";
            goldTotal.text = "";
            enemiesTotal.text ="" ;
            itemsTotal.text = "";
            levelText.text = "Floor " + level;
            messageIntro.text = nickname+" came to The Dungeon, dreaming of gold and adventures! Will he find that?";
        }
        //changing map measures
        incrementBoard = Random.Range(0,10);
        boardScript.wallCountMax = incrementBoard+12;
        boardScript.wallCountMin = 5+incrementBoard;
        boardScript.foodCountMin = 0;
        boardScript.foodCountMax = 5;
        boardScript.harmfulObjetcsCountMin = 2+incrementBoard;
        boardScript.harmfulObjetcsCountMax = 5+incrementBoard;
        boardScript.columnsBoard = incrementBoard+8;
        boardScript.rowsBoard = incrementBoard+8;
        boardScript.enemiesCountMin = 1+incrementBoard;
        boardScript.enemiesCountMax = incrementBoard+8;
        boardScript.goldCountMin = 0;
        boardScript.goldCountMax = incrementBoard;
    
        doingSetup = true;
        //get texts from UI
        levelImage = GameObject.Find("LevelImage");
        levelText = GameObject.Find("LevelText").GetComponent<Text>();
        goldTotal = GameObject.Find("TotalGoldGame").GetComponent<Text>();
        enemiesTotal = GameObject.Find("TotalEnemiesEliminated").GetComponent<Text>();
        itemsTotal = GameObject.Find("TotalItemsObtained").GetComponent<Text>();
        bloodPointsTotal = GameObject.Find("TotalBloodPoints").GetComponent<Text>();
        messageIntro = GameObject.Find("MessageIntro").GetComponent<Text>();
        //update texts from UI
        if (instance.level !=1)
        {
            levelText.text = "Floor " + level;
            bloodPointsTotal.text = "BloodPoints: " + bloodPointsNew;
            goldTotal.text = "Gold: " + gameGoldObtained;
            enemiesTotal.text = "Enemies eliminated: " + gameEnemiesEliminated;
            itemsTotal.text = "Items obtained: " + gameItemsObtained;
            messageIntro.text = "";
        }
        //activated black screen
        levelImage.GetComponent<Image>().enabled = true;
        Invoke("HideLevelImage", levelStartDelay);
        enemies.Clear();
        boardScript.SetupScene(level);
    }

    /// <summary>
    ///call thread for load data player and load items 
    /// </summary>
    public void resetItems()
    {
        Thread loadDataPlayer = new Thread(new ThreadStart(threadLoadDataPlayer));
        loadDataPlayer.Start();
        loadDataPlayer.Join();
        loadItems();
    }

    /// <summary>
    ///function for desactivate black screen
    /// </summary>
    void HideLevelImage()
    {
        
        if (SceneManager.GetActiveScene().buildIndex == 4)
        {
            levelImage.GetComponent<Image>().enabled = false;
            levelText.text = "";
            goldTotal.text = "";
            enemiesTotal.text = "";
            itemsTotal.text = "";
            bloodPointsTotal.text = "";
            messageIntro.text = "";
            doingSetup = false;
        }
        


    }

    /// <summary>
    ///call thread for update game data
    /// </summary>
    public void updateGame()
    {
        Thread updateGame = new Thread(new ThreadStart(threadUpdateGame));
        updateGame.Start();
        updateGame.Join();
    }

    /// <summary>
    ///function when the player dies
    /// </summary>
    public void GameOver()
    {
        //update data
        calculateBloodPoints();
        updateBloodPoints();
        updateGame();
        updateCharacter();
        //update texts
        goldTotal.text = "Gold: " + gameGoldObtained;
        enemiesTotal.text = "Enemies eliminated: " + gameEnemiesEliminated;
        itemsTotal.text = "Items obtained: " + gameItemsObtained;
        bloodPointsTotal.text = "BloodPoints: " + bloodPointsNew;
        levelText.text = nickname + " died in the floor " + level + " ,never to be found again";
        //activated black screen
        levelImage.GetComponent<Image>().enabled = true;
        //stop sound
        SoundManager.instance.efxSource.Pause();
        SoundManager.instance.musicSource.Pause();
        // call functions after 3 seconds
        Invoke("LoadScene",3);
        Invoke("destroyGame",3);
        messageIntro.text = "";
    }

    /// <summary>
    ///function for destroy GameManager 
    /// </summary>
    void destroyGame()
    {
        Destroy(gameObject);
    }

    /// <summary>
    ///function for load scene (create character)
    /// </summary>
    void LoadScene()
    {
        scene = SceneManager.GetActiveScene().buildIndex - 1;
        SceneManager.LoadScene(scene);
    }

    /// <summary>
    ///function for destroy GameManager 
    /// </summary>
    void Update()
    {
        // Update is called once per frame
        if (playersTurn || enemiesMoving|| doingSetup )
        {
            return;
        }
        //call function for move enemies
        StartCoroutine(MoveEnemies());
    }

    /// <summary>
    ///function for add enemies to list
    /// </summary>
    public void AddEnemiesToList(Enemy script)
    {
        enemies.Add(script);
    }

    /// <summary>
    ///coroutine for move enemies
    /// </summary>
    IEnumerator MoveEnemies()
    {
        enemiesMoving = true;
        yield return new WaitForSeconds(turnDelay);
        if(enemies.Count == 0)
        {
            yield return new WaitForSeconds(turnDelay);
        }
        for(int i = 0; i < enemies.Count; i++)
        {
            if (!enemies[i].alive)
            {
                enemies.RemoveAt(i);
            }
            else
            {
                enemies[i].MoveEnemy();
                yield return new WaitForSeconds(enemies[i].moveTime);
            }

        }
        playersTurn = true;
        enemiesMoving = false;
    }

}

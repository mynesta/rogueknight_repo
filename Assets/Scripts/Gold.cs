﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gold : MonoBehaviour
{
    //variables
    public int quantity;
    public int minQuantity = 1;
    public int maxQuantity = 11;
    /// <summary>
    ///start gold
    /// </summary>
    void Start()
    {
        //calculate gold
        quantity = UnityEngine.Random.Range(minQuantity,maxQuantity );
    }

}

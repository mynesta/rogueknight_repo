﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader : MonoBehaviour
{
    //variables
    public GameObject gateManager;
    private GameObject player;
    /// <summary>
    /// first function called
    /// </summary>
    private void Awake()
    {
        if(GameManager.instance == null)
        {
            //generate GameManager
            Instantiate(gateManager);
        }

    }


}

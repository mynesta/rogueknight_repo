﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    //variables
    public Text nicknameText;
    public Text level;
    public Text hitPoints;
    public Text attack;
    public Text defense;
    public Text inventory;
    private int scene;
    public int playerLifePoints;
    public int maxPlayerLifePoints;
    public int playerAttack;
    public int playerDefense;
    public string nickname;
    public int playerLevel;
    public int inventorySlots;
    public int experience;
    public int needExperience;
    public int gold;
    private int characterID;
    private int itemsCount;
    public string user;
    public string password;
    public int gameCheckpoint;
    public int gameGoldObtained;
    public int gameItemsObtained;
    public int gameExperienceObtained;
    public int gameEnemiesEliminated;
    private int gameID;
    private int iterationItemLoad;
    public GameObject item;
    private int itemsCountForLimit;
    private int iterationItem;
    private int idItem;
    private string nameItem;
    private int quantityItem;
    private bool equipItem;

    public GameObject[] itemsTiles;

    private Transform inventoryUI;
    public List<InventoryItem> items = new List<InventoryItem>();

    private string url = "mysqlantimateria.ddns.net";
    private int port = 12345;

    /// <summary>
    ///start main menu
    /// </summary>
    private void Start()
    {
        //load data
        GameManager gameManager = new GameManager();
        gameManager.initialThreads();
        nicknameText.text = gameManager.nickname;
        //update texts from data player
        level.text = Convert.ToString(gameManager.playerLevel);
        hitPoints.text = Convert.ToString(gameManager.playerLifePoints);
        attack.text = Convert.ToString(gameManager.playerAttack);
        defense.text = Convert.ToString(gameManager.playerDefense);
        inventory.text = Convert.ToString(gameManager.inventorySlots);
        //get user
        user = Login.user;
        password = Login.pass;
        //load data player and game
        inventoryUI = GameObject.Find("InventoryLayoutMainMenu").GetComponent<Transform>();
        Thread loadDataPlayer = new Thread(new ThreadStart(threadLoadDataPlayer));
        loadDataPlayer.Start();
        loadDataPlayer.Join();
        Thread loadDataGame = new Thread(new ThreadStart(threadLoadDataGame));
        loadDataGame.Start();
        loadDataGame.Join();
        //load inventory
        loadItems();
        loadItemsUI();

    }

    /// <summary>
    ///function to button enter dungeon, change scene
    /// </summary>
    public void startGame()
    {
        scene = SceneManager.GetActiveScene().buildIndex + 2;
        SceneManager.LoadScene(scene);
        GameManager.instance.resetItems();
    }

    /// <summary>
    ///exit of application
    /// </summary>
    public void exitGame()
    {
        Application.Quit();
    }

    /// <summary>
    ///change screen to the records
    /// </summary>
    public void records()
    {
        scene = SceneManager.GetActiveScene().buildIndex + 4;
        SceneManager.LoadScene(scene);
    }

    /// <summary>
    ///thread to load data player
    /// </summary>
    private void threadLoadDataPlayer()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("select", clientSocket);
            sendString("player", clientSocket);
            //load data player
            sendString(user, clientSocket);
            if (Convert.ToBoolean(receiveString(clientSocket)))
            {
                characterID = Convert.ToInt32(receiveString(clientSocket));
                nickname = receiveString(clientSocket);
                playerLevel = Convert.ToInt32(receiveString(clientSocket));
                int lifeDB = Convert.ToInt32(receiveString(clientSocket));
                playerLifePoints = lifeDB;
                maxPlayerLifePoints = lifeDB;
                playerAttack = Convert.ToInt32(receiveString(clientSocket));
                playerDefense = Convert.ToInt32(receiveString(clientSocket));
                inventorySlots = Convert.ToInt32(receiveString(clientSocket));
                experience = Convert.ToInt32(receiveString(clientSocket));
                needExperience = Convert.ToInt32(receiveString(clientSocket));
                gold = Convert.ToInt32(receiveString(clientSocket));
                itemsCount = Convert.ToInt32(receiveString(clientSocket));
            }


        }
    }

    /// <summary>
    ///function for receive data from server
    /// </summary>
    /// <param name="clientSocket"></param>
    /// <returns>rcv</returns>
    private String receiveString(Socket clientSocket)
    {
        byte[] rcvLenBytes = new byte[4];
        clientSocket.Receive(rcvLenBytes);
        int rcvLen = System.BitConverter.ToInt32(rcvLenBytes, 0);
        byte[] rcvBytes = new byte[rcvLen];
        clientSocket.Receive(rcvBytes);
        String rcv = System.Text.Encoding.ASCII.GetString(rcvBytes);
        return rcv;
    }

    /// <summary>
    ///function for send data to server
    /// </summary>
    /// <param name="toSend"></param>
    /// <param name="clientSocket"></param>
    private void sendString(String toSend, Socket clientSocket)
    {
        int toSendLen = System.Text.Encoding.ASCII.GetByteCount(toSend);
        byte[] toSendBytes = System.Text.Encoding.ASCII.GetBytes(toSend);
        byte[] toSendLenBytes = System.BitConverter.GetBytes(toSendLen);
        clientSocket.Send(toSendLenBytes);
        clientSocket.Send(toSendBytes);
    }

    /// <summary>
    ///thread to load game
    /// </summary>
    private void threadLoadDataGame()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("select", clientSocket);
            sendString("game", clientSocket);
            //load data game
            gameID = Convert.ToInt32(receiveString(clientSocket));
            gameCheckpoint = Convert.ToInt32(receiveString(clientSocket));
            gameGoldObtained = Convert.ToInt32(receiveString(clientSocket));
            gameItemsObtained = Convert.ToInt32(receiveString(clientSocket));
            gameExperienceObtained = Convert.ToInt32(receiveString(clientSocket));
            gameEnemiesEliminated = Convert.ToInt32(receiveString(clientSocket));
        }
    }

    /// <summary>
    ///function to load items in a list
    /// </summary>
    public void loadItems()
    {
        items.Clear();
        for (iterationItemLoad = 0; iterationItemLoad < itemsCount; iterationItemLoad++)
        {
            Thread loadDataItems = new Thread(new ThreadStart(threadLoadItemInventory));
            loadDataItems.Start();
            loadDataItems.Join();
            //create object
            InventoryItem inventoryItem = new InventoryItem(nameItem, idItem, itemsTiles[idItem - 1].GetComponent<SpriteRenderer>().sprite,
                quantityItem, equipItem, itemsTiles[idItem - 1].GetComponent<Item>().sellCost, itemsTiles[idItem - 1].GetComponent<Item>().buyCost);
            items.Add(inventoryItem);
        }
    }

    /// <summary>
    ///thread to load items inventory
    /// </summary>
    private void threadLoadItemInventory()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("select", clientSocket);
            sendString("itemInventory", clientSocket);
            //load item
            sendString(Convert.ToString(characterID), clientSocket);
            sendString(Convert.ToString(iterationItemLoad), clientSocket);
            idItem = Convert.ToInt32(receiveString(clientSocket));
            nameItem = receiveString(clientSocket);
            quantityItem = Convert.ToInt32(receiveString(clientSocket));
            equipItem = Convert.ToBoolean(receiveString(clientSocket));
        }
    }

    /// <summary>
    ///function to load items in the UI
    /// </summary>
    public void loadItemsUI()
    {
        //count items
        itemsCountForLimit = 0;
        for (int i = 0; i < items.Count; i++)
        {
            for (int l = 0; l < (items[i].quantity); l++)
            {
                if (items[i].quantity > 0)
                {
                    itemsCountForLimit += 1;
                    //load image item
                    item.GetComponent<Image>().sprite = items[i].sprite;
                    //instantiate item
                    GameObject instantiated = Instantiate(item, inventoryUI);
                }
            }
        }
    }
}

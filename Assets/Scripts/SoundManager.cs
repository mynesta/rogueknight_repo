﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    //variables
    public AudioSource efxSource;
    public AudioSource musicSource;
    public static SoundManager instance = null;

    public float lowPitchRange = 0.95f;
    public float highPitchRange = 1.05f;
    public bool canActivate;

    /// <summary>
    ///first function called
    /// </summary>
    void Awake()
    {
        //create instance
        canActivate = true;
        if(instance == null)
        {
            instance = this;
        }
        else if(instance != null)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    ///function to play audio clip
    /// </summary>
    /// <param name="clip"></param>
    public void PlaySingle (AudioClip clip)
    {
        efxSource.clip = clip;
        efxSource.Play();
    }

    /// <summary>
    ///function to random pitch,random sounds and play
    /// </summary>
    /// <param name="clips"></param>
    public void RandomizeSfx (params AudioClip [] clips)
    {
        if (canActivate)
        {
            int randomIndex = Random.Range(0, clips.Length);
            float randomPitch = Random.Range(lowPitchRange, highPitchRange);

            efxSource.pitch = randomPitch;
            efxSource.clip = clips[randomIndex];
            efxSource.Play();
        }
        
    }


    
}

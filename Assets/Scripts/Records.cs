﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Records : MonoBehaviour
{
    //variables
    int scene;

    private string user;
    private string password;
    private string positionPlayer;
    private string scorePlayer;
    List<List<string>> topFive = new List<List<string>>();
    List<string> TopPlayer;

    private string url = "mysqlantimateria.ddns.net";
    private int port = 12345;

    private Text namePlayer1;
    private Text namePlayer2;
    private Text namePlayer3;
    private Text namePlayer4;
    private Text namePlayer5;

    private Text scorePlayer1;
    private Text scorePlayer2;
    private Text scorePlayer3;
    private Text scorePlayer4;
    private Text scorePlayer5;

    private Text positionPlayerText;
    private Text namePlayerText;
    private Text scorePlayerText;


    /// <summary>
    /// start records
    /// </summary>
    void Start()
    {
        //get user
        user = Login.user;
        password = Login.pass;
        Thread loadRecords = new Thread(new ThreadStart(threadloadRecords));
        loadRecords.Start();
        loadRecords.Join();
        //get texts top 5
        namePlayer1 = GameObject.Find("PlayerText1").GetComponent<Text>();
        namePlayer2 = GameObject.Find("PlayerText2").GetComponent<Text>();
        namePlayer3 = GameObject.Find("PlayerText3").GetComponent<Text>();
        namePlayer4 = GameObject.Find("PlayerText4").GetComponent<Text>();
        namePlayer5 = GameObject.Find("PlayerText5").GetComponent<Text>();
        scorePlayer1 = GameObject.Find("ScoreText1").GetComponent<Text>();
        scorePlayer2 = GameObject.Find("ScoreText2").GetComponent<Text>();
        scorePlayer3 = GameObject.Find("ScoreText3").GetComponent<Text>();
        scorePlayer4 = GameObject.Find("ScoreText4").GetComponent<Text>();
        scorePlayer5 = GameObject.Find("ScoreText5").GetComponent<Text>();
        //get text user
        positionPlayerText = GameObject.Find("PositionPlayerText").GetComponent<Text>();
        namePlayerText = GameObject.Find("NamePlayerText").GetComponent<Text>();
        scorePlayerText = GameObject.Find("ScorePlayerText").GetComponent<Text>();
        //load data player
        positionPlayerText.text = positionPlayer;
        namePlayerText.text = user;
        scorePlayerText.text = scorePlayer;
        //load data top5
        namePlayer1.text = topFive[0][0];
        namePlayer2.text = topFive[1][0];
        namePlayer3.text = topFive[2][0];
        namePlayer4.text = topFive[3][0];
        namePlayer5.text = topFive[4][0];
        scorePlayer1.text = topFive[0][1];
        scorePlayer2.text = topFive[1][1];
        scorePlayer3.text = topFive[2][1];
        scorePlayer4.text = topFive[3][1];
        scorePlayer5.text = topFive[4][1];
    }

    /// <summary>
    /// function to back main menu
    /// </summary>
    public void backMainMenu()
    {
        scene = SceneManager.GetActiveScene().buildIndex - 4;
        SceneManager.LoadScene(scene);
    }

    /// <summary>
    /// thread to load records
    /// </summary>
    private void threadloadRecords()
    {
        //communication with server and connect
        IPHostEntry Hosts = Dns.GetHostEntry(url);
        IPEndPoint serverAddress = new IPEndPoint(IPAddress.Parse(Hosts.AddressList[0].ToString()), port);
        Socket clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        clientSocket.Connect(serverAddress);
        //verification user
        sendString(user, clientSocket);
        sendString(password, clientSocket);
        String stringServer = receiveString(clientSocket);
        if (stringServer == "CorrectUser")
        {
            // choose action for the database
            sendString("select", clientSocket);
            sendString("records", clientSocket);
            //send user name
            sendString(user, clientSocket);
            //load data player
            positionPlayer = receiveString(clientSocket);
            scorePlayer = receiveString(clientSocket);
            //load data top 5
            topFive.Clear();
            for (int i = 0; i < 5; i++)
            {
                TopPlayer = new List<string>();
                TopPlayer.Add(receiveString(clientSocket));
                TopPlayer.Add(receiveString(clientSocket));
                topFive.Add(TopPlayer);
            }

           
        }
    }

    /// <summary>
    ///function for receive data from server
    /// </summary>
    /// <param name="clientSocket"></param>
    /// <returns>rcv</returns>
    private String receiveString(Socket clientSocket)
    {
        byte[] rcvLenBytes = new byte[4];
        clientSocket.Receive(rcvLenBytes);
        int rcvLen = System.BitConverter.ToInt32(rcvLenBytes, 0);
        byte[] rcvBytes = new byte[rcvLen];
        clientSocket.Receive(rcvBytes);
        String rcv = System.Text.Encoding.ASCII.GetString(rcvBytes);
        return rcv;
    }

    /// <summary>
    ///function for send data to server
    /// </summary>
    /// <param name="toSend"></param>
    /// <param name="clientSocket"></param>
    private void sendString(String toSend, Socket clientSocket)
    {
        int toSendLen = System.Text.Encoding.ASCII.GetByteCount(toSend);
        byte[] toSendBytes = System.Text.Encoding.ASCII.GetBytes(toSend);
        byte[] toSendLenBytes = System.BitConverter.GetBytes(toSendLen);
        clientSocket.Send(toSendLenBytes);
        clientSocket.Send(toSendBytes);
    }
}

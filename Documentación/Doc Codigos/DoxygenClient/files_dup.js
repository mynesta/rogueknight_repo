var files_dup =
[
    [ "BoardManager.cs", "_board_manager_8cs.html", "_board_manager_8cs" ],
    [ "ButtonsMove.cs", "_buttons_move_8cs.html", [
      [ "ButtonsMove", "class_buttons_move.html", "class_buttons_move" ]
    ] ],
    [ "CreateCharacter.cs", "_create_character_8cs.html", [
      [ "CreateCharacter", "class_create_character.html", "class_create_character" ]
    ] ],
    [ "Enemy.cs", "_enemy_8cs.html", [
      [ "Enemy", "class_enemy.html", "class_enemy" ]
    ] ],
    [ "GameManager.cs", "_game_manager_8cs.html", "_game_manager_8cs" ],
    [ "Gold.cs", "_gold_8cs.html", [
      [ "Gold", "class_gold.html", "class_gold" ]
    ] ],
    [ "InventoryItem.cs", "_inventory_item_8cs.html", [
      [ "InventoryItem", "class_inventory_item.html", "class_inventory_item" ]
    ] ],
    [ "Item.cs", "_item_8cs.html", [
      [ "Item", "class_item.html", "class_item" ]
    ] ],
    [ "LeaveInGame.cs", "_leave_in_game_8cs.html", [
      [ "LeaveInGame", "class_leave_in_game.html", "class_leave_in_game" ]
    ] ],
    [ "Loader.cs", "_loader_8cs.html", [
      [ "Loader", "class_loader.html", "class_loader" ]
    ] ],
    [ "Login.cs", "_login_8cs.html", [
      [ "Login", "class_login.html", "class_login" ]
    ] ],
    [ "MainMenu.cs", "_main_menu_8cs.html", [
      [ "MainMenu", "class_main_menu.html", "class_main_menu" ]
    ] ],
    [ "MovingObject.cs", "_moving_object_8cs.html", [
      [ "MovingObject", "class_moving_object.html", "class_moving_object" ]
    ] ],
    [ "Player.cs", "_player_8cs.html", [
      [ "Player", "class_player.html", "class_player" ]
    ] ],
    [ "Records.cs", "_records_8cs.html", [
      [ "Records", "class_records.html", "class_records" ]
    ] ],
    [ "Register.cs", "_register_8cs.html", [
      [ "Register", "class_register.html", "class_register" ]
    ] ],
    [ "Shop.cs", "_shop_8cs.html", [
      [ "Shop", "class_shop.html", "class_shop" ]
    ] ],
    [ "SoundManager.cs", "_sound_manager_8cs.html", [
      [ "SoundManager", "class_sound_manager.html", "class_sound_manager" ]
    ] ],
    [ "Tutorial.cs", "_tutorial_8cs.html", [
      [ "Tutorial", "class_tutorial.html", "class_tutorial" ]
    ] ],
    [ "Wall.cs", "_wall_8cs.html", [
      [ "Wall", "class_wall.html", "class_wall" ]
    ] ]
];
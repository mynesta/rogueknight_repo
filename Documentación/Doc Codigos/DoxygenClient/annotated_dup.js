var annotated_dup =
[
    [ "BoardManager", "class_board_manager.html", "class_board_manager" ],
    [ "ButtonsMove", "class_buttons_move.html", "class_buttons_move" ],
    [ "CreateCharacter", "class_create_character.html", "class_create_character" ],
    [ "Enemy", "class_enemy.html", "class_enemy" ],
    [ "GameManager", "class_game_manager.html", "class_game_manager" ],
    [ "Gold", "class_gold.html", "class_gold" ],
    [ "InventoryItem", "class_inventory_item.html", "class_inventory_item" ],
    [ "Item", "class_item.html", "class_item" ],
    [ "LeaveInGame", "class_leave_in_game.html", "class_leave_in_game" ],
    [ "Loader", "class_loader.html", "class_loader" ],
    [ "Login", "class_login.html", "class_login" ],
    [ "MainMenu", "class_main_menu.html", "class_main_menu" ],
    [ "MovingObject", "class_moving_object.html", "class_moving_object" ],
    [ "Player", "class_player.html", "class_player" ],
    [ "Records", "class_records.html", "class_records" ],
    [ "Register", "class_register.html", "class_register" ],
    [ "Shop", "class_shop.html", "class_shop" ],
    [ "SoundManager", "class_sound_manager.html", "class_sound_manager" ],
    [ "Tutorial", "class_tutorial.html", "class_tutorial" ],
    [ "Wall", "class_wall.html", "class_wall" ]
];
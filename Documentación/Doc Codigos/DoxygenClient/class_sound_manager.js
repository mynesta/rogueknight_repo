var class_sound_manager =
[
    [ "PlaySingle", "class_sound_manager.html#a5ad1175ce8acad3b9d38c0f0275ad066", null ],
    [ "RandomizeSfx", "class_sound_manager.html#af353baebc3f1704fdaadfa3997220895", null ],
    [ "canActivate", "class_sound_manager.html#a5ab1a973cebcf4c22562bafda75ab73d", null ],
    [ "efxSource", "class_sound_manager.html#a1c882777e273c8a20186e666fa4a2612", null ],
    [ "highPitchRange", "class_sound_manager.html#aa8952cdd0fa0e72028f862b1d70454d2", null ],
    [ "lowPitchRange", "class_sound_manager.html#a85e05fc68a861b5b2d147a898d1ca221", null ],
    [ "musicSource", "class_sound_manager.html#a173b7c95fdd6796d7c1f268e23fcfc84", null ]
];
var hierarchy =
[
    [ "BoardManager.Count", "class_board_manager_1_1_count.html", null ],
    [ "InventoryItem", "class_inventory_item.html", null ],
    [ "MonoBehaviour", null, [
      [ "BoardManager", "class_board_manager.html", null ],
      [ "ButtonsMove", "class_buttons_move.html", null ],
      [ "CreateCharacter", "class_create_character.html", null ],
      [ "GameManager", "class_game_manager.html", null ],
      [ "Gold", "class_gold.html", null ],
      [ "Item", "class_item.html", null ],
      [ "LeaveInGame", "class_leave_in_game.html", null ],
      [ "Loader", "class_loader.html", null ],
      [ "Login", "class_login.html", null ],
      [ "MainMenu", "class_main_menu.html", null ],
      [ "MovingObject", "class_moving_object.html", [
        [ "Enemy", "class_enemy.html", null ],
        [ "Player", "class_player.html", null ]
      ] ],
      [ "Records", "class_records.html", null ],
      [ "Register", "class_register.html", null ],
      [ "Shop", "class_shop.html", null ],
      [ "SoundManager", "class_sound_manager.html", null ],
      [ "Tutorial", "class_tutorial.html", null ],
      [ "Wall", "class_wall.html", null ]
    ] ]
];
/* Base de datos Rogue Knight */

# Creamos la base de datos

DROP DATABASE IF EXISTS ROGUEKNIGHT;
CREATE DATABASE ROGUEKNIGHT;
DROP USER IF EXISTS RogueAdmin;
DROP USER IF EXISTS RogueReader;
DROP USER IF EXISTS RogueExec;
CREATE USER RogueAdmin IDENTIFIED BY 'RoguePass';
CREATE USER RogueReader IDENTIFIED BY 'ReadData';
CREATE USER RogueExec IDENTIFIED BY 'ExecQueries';
GRANT ALL PRIVILEGES ON ROGUEKNIGHT.* TO RogueAdmin WITH GRANT OPTION;
GRANT SELECT ON ROGUEKNIGHT.* TO RogueReader WITH GRANT OPTION;
GRANT EXECUTE ON ROGUEKNIGHT.* TO RogueExec WITH GRANT OPTION;
USE ROGUEKNIGHT;

# Creamos las tablas

CREATE TABLE USERDATABASE (
	username VARCHAR(15) PRIMARY KEY,
	email VARCHAR(30),
	birthDate VARCHAR(30),
	password VARCHAR(30),
	bloodPoints INT
);

CREATE TABLE CHARACTERPLAYABLE (
	characterPlayableID INT PRIMARY KEY,
	alive BOOLEAN,
	nickName VARCHAR(30),
	characterLevel INT,
	hitPoints INT,
	attack INT,
	defense INT,
	inventorySlots INT,
	experience INT,
	neededExperience INT,
	gold INT,
	username VARCHAR(15)
);

CREATE TABLE GAME(
	gameID INT PRIMARY KEY,
	finished BOOLEAN,
	checkpoint INT,
	goldObtained INT,
	itemsObtained INT,
	finalFloor INT,
	experienceObtained INT,
	enemiesEliminated INT,
	characterPlayableID INT
);

CREATE TABLE ITEM (
	itemID INT PRIMARY KEY,
	name VARCHAR(30),
	effect VARCHAR(50),
	equip BOOLEAN,
	buyingCost INT,
	sellingCost INT
);

CREATE TABLE PLAYERINVENTORY (
	characterPlayableID INT,
	itemID INT,
	quantity INT,
	PRIMARY KEY(characterPlayableID, itemID)
);

# Creamos las claves foraneas

ALTER TABLE PLAYERINVENTORY ADD CONSTRAINT fk_PLAYERINVENTORY_characterPlayableID FOREIGN KEY (characterPlayableID) REFERENCES CHARACTERPLAYABLE(characterPlayableID) ON DELETE CASCADE;
ALTER TABLE PLAYERINVENTORY ADD CONSTRAINT fk_PLAYERINVENTORY_itemID FOREIGN KEY (itemID) REFERENCES ITEM(itemID) ON DELETE CASCADE;
ALTER TABLE GAME ADD CONSTRAINT fk_GAME_characterPlayableID FOREIGN KEY (characterPlayableID) REFERENCES CHARACTERPLAYABLE(characterPlayableID) ON DELETE SET NULL;
ALTER TABLE CHARACTERPLAYABLE ADD CONSTRAINT fk_CHARACTERPLAYABLE_username FOREIGN KEY (username) REFERENCES USERDATABASE(username) ON DELETE SET NULL;

# Insertamos los datos

INSERT INTO USERDATABASE VALUES ('lowscojownes','miscojownes@emilio.jdr','21-02-450AC','jajaequisde',0);
INSERT INTO USERDATABASE VALUES ('manuelhacker','tehackeo@emilio.jdr','21-02-200AC','merioentucara',0);

INSERT INTO CHARACTERPLAYABLE VALUES (1,FALSE,'MascaEscrotos',1,100,10,5,3,0,100,0,'lowscojownes');
INSERT INTO CHARACTERPLAYABLE VALUES (2,TRUE,'EmperadorMachacasaurio',1,100,10,5,3,0,100,0,'lowscojownes');
INSERT INTO CHARACTERPLAYABLE VALUES (3,TRUE,'Hackerman',1,100,10,5,3,0,100,0,'manuelhacker');

INSERT INTO GAME VALUES (1,TRUE,0,0,0,0,0,0,1);
INSERT INTO GAME VALUES (2,TRUE,0,20,12,10,55,30,2);
INSERT INTO GAME VALUES (3,FALSE,15,30,1,15,60,50,2);
INSERT INTO GAME VALUES (4,FALSE,25,50,60,80,50,70,3);

INSERT INTO ITEM VALUES (1,'Leather Armor','It grants +25% Defense',TRUE,150,60);
INSERT INTO ITEM VALUES (2,'Plate mail','It grants +40% Defense',TRUE,450,150);
INSERT INTO ITEM VALUES (3,'Potion','It restores 5 HP',FALSE,10,4);
INSERT INTO ITEM VALUES (4,'Quality Potion','It restores 15 HP',FALSE,30,10);
INSERT INTO ITEM VALUES (5,'Heroic Potion','It restores 40 HP',FALSE,80,20);
INSERT INTO ITEM VALUES (6,'Divine Potion','It restores 100 HP',FALSE,150,50);
INSERT INTO ITEM VALUES (7,'Broadsword','It grants +25% Attack',TRUE,150,60);
INSERT INTO ITEM VALUES (8,'War Axe','It grants +40% Attack',TRUE,450,150);

INSERT INTO PLAYERINVENTORY VALUES (1,2,2);
INSERT INTO PLAYERINVENTORY VALUES (2,2,1);
INSERT INTO PLAYERINVENTORY VALUES (2,3,3);
INSERT INTO PLAYERINVENTORY VALUES (3,2,1);
INSERT INTO PLAYERINVENTORY VALUES (3,1,4);

#creamos las funciones para el usuario

DELIMITER // 
CREATE PROCEDURE insertUser(usernameV CHAR(15), emailV VARCHAR(30), birthDateV VARCHAR(30), passwordV VARCHAR(30))
BEGIN 
	INSERT USERDATABASE SET username = usernameV, email = emailV, birthDate = birthDateV, password = passwordV, bloodPoints = 0;
END;
// 
DELIMITER ;


DELIMITER // 
CREATE PROCEDURE insertCharacterplayable(aliveV BOOLEAN, nickNameV VARCHAR(30), characterLevelV INT, hitPointsV INT,
attackV INT,defenseV INT,inventorySlotsV INT,experienceV INT,neededExperienceV INT,goldV INT,usernameV VARCHAR(15))
BEGIN
	DECLARE characterPlayableIDV INT;
	SELECT COUNT(*) FROM CHARACTERPLAYABLE INTO characterPlayableIDV;
	SET characterPlayableIDV=characterPlayableIDV+1; 
	INSERT CHARACTERPLAYABLE SET characterPlayableID = characterPlayableIDV, alive = aliveV, nickName = nickNameV,
	characterLevel = characterLevelV, hitPoints = hitPointsV, attack = attackV,defense = defenseV,
	inventorySlots = inventorySlotsV,experience = experienceV,neededExperience = neededExperienceV,gold = goldV,username = usernameV;
END;
// 
DELIMITER ;
 

DELIMITER // 
CREATE PROCEDURE updateItemInventory(characterPlayableIDV INT,itemIDV INT,quantityV INT)
BEGIN
	DECLARE itemExist INT;
	DECLARE itemEquip BOOLEAN;
	SELECT equip FROM ITEM WHERE itemID =itemIDV INTO itemEquip;
	SELECT COUNT(*) FROM PLAYERINVENTORY WHERE characterPlayableID = characterPlayableIDV AND itemID =itemIDV INTO itemExist;
	IF (quantityV = 0) THEN
		DELETE FROM PLAYERINVENTORY WHERE characterPlayableID = characterPlayableIDV AND itemID =itemIDV;
	ELSEIF (itemExist<1 AND itemEquip = FALSE) THEN
		INSERT PLAYERINVENTORY SET characterPlayableID = characterPlayableIDV, itemID = itemIDV, quantity = quantityV;
	ELSEIF(itemExist<1 AND itemEquip = TRUE) THEN
		INSERT PLAYERINVENTORY SET characterPlayableID = characterPlayableIDV, itemID = itemIDV, quantity = 1;
	ELSEIF (itemExist>0 AND itemEquip = FALSE) THEN
		UPDATE PLAYERINVENTORY SET quantity = quantityV WHERE characterPlayableID = characterPlayableIDV AND itemID =itemIDV;
	ELSEIF (itemExist>0 AND itemEquip = TRUE) THEN
		UPDATE PLAYERINVENTORY SET quantity = 1 WHERE characterPlayableID = characterPlayableIDV AND itemID =itemIDV;
	END IF;
END;
// 
DELIMITER ;

DELIMITER // 
CREATE PROCEDURE insertGame(finishedV BOOLEAN, checkpointV INT, goldObtainedV INT,itemsObtainedV INT,finalFloorV INT,
experienceObtainedV INT,enemiesEliminatedV INT,characterPlayableIDV INT)
BEGIN 
	DECLARE gameIDV INT;
	SELECT COUNT(*) FROM GAME INTO gameIDV;
	SET gameIDV=gameIDV+1;
	INSERT GAME SET gameID = gameIDV, finished = finishedV, checkpoint = checkpointV, goldObtained = goldObtainedV,
	itemsObtained = itemsObtainedV,finalFloor = finalFloorV,experienceObtained = experienceObtainedV,enemiesEliminated = enemiesEliminatedV,
	characterPlayableID = characterPlayableIDV;
END;
// 
DELIMITER ;

 
DELIMITER // 
CREATE PROCEDURE updateGame(gameIDV INT,finishedV BOOLEAN, checkpointV INT, goldObtainedV INT,itemsObtainedV INT,finalFloorV INT,
experienceObtainedV INT,enemiesEliminatedV INT)
BEGIN 
	UPDATE GAME SET finished = finishedV, checkpoint = checkpointV, goldObtained = goldObtainedV,
	itemsObtained = itemsObtainedV,finalFloor = finalFloorV,experienceObtained = experienceObtainedV,
	enemiesEliminated = enemiesEliminatedV WHERE gameID = gameIDV;
END;
// 
DELIMITER ;


DELIMITER // 
CREATE PROCEDURE updateCharacterplayable(characterPlayableIDV INT,aliveV BOOLEAN, characterLevelV INT, hitPointsV INT,
attackV INT,defenseV INT,inventorySlotsV INT,experienceV INT,neededExperienceV INT,goldV INT)
BEGIN
	UPDATE CHARACTERPLAYABLE SET alive = aliveV,characterLevel = characterLevelV, hitPoints = hitPointsV,
	attack = attackV,defense = defenseV,inventorySlots = inventorySlotsV,experience = experienceV,
	neededExperience = neededExperienceV,gold = goldV WHERE characterPlayableID = characterPlayableIDV;
END;
// 
DELIMITER;


DELIMITER // 
CREATE PROCEDURE updateUser(usernameV CHAR(15),bloodPointsV INT)
BEGIN 
	UPDATE USERDATABASE SET bloodPoints = bloodPointsV WHERE username = usernameV;
END;
// 
DELIMITER ;


# Mostramos los datos

SELECT * FROM USERDATABASE;
SELECT * FROM CHARACTERPLAYABLE;
SELECT * FROM GAME;
SELECT * FROM PLAYERINVENTORY;
SELECT * FROM ITEM;